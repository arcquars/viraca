import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {DbService} from './services/db.service';
import {AuthenticationService} from './services/authentication.service';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    navigate: any;
    public username = '';
    public role = '';

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private db: DbService,
        private storage: Storage,
        private authenticationService: AuthenticationService,
        private router: Router,
        private authService: AuthenticationService,
        private statusBar: StatusBar
    ) {
        this.sideMenu();
        this.initializeApp();

        this.platform.ready().then(() => {
            db.openDb();
        }).catch(error => {
            console.log(error);
        });
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.authenticationService.authenticationState.subscribe(state => {
                if (state) {
                    this.router.navigate(['proyecto']);
                } else {
                    this.router.navigate(['login']);
                }
            });

            this.authenticationService.authenticationName.subscribe(username => {
                this.username = username;
            });

            this.authenticationService.authenticationRol.subscribe((rol: string) => {
                if (rol === 'ADMIN') {
                    this.role = 'ADMINISTRADOR';
                } else {
                    this.role = 'ADMINISTRADOR DE PROYECTO';
                }
                this.sideMenu();
            });

        });
    }

    sideMenu() {
        let menuAux = [];
        switch (this.role) {
            case 'ADMINISTRADOR':
                menuAux = [
                    {
                        title: 'Proyectos',
                        url: '/proyecto',
                        icon: 'newspaper-outline'
                    },
                    {
                        title: 'Personas',
                        url: '/persons',
                        icon: 'people-outline'
                    },
                    {
                        title: 'Administradores Proyecto',
                        url: '/managers',
                        icon: 'people-circle-outline'
                    }
                ];
                break;
            default:
                menuAux = [
                    {
                        title: 'Proyectos',
                        url: '/proyecto',
                        icon: 'newspaper-outline'
                    },
                ];
                break;
        }
        this.navigate = menuAux;

    }

    logout() {
        this.authService.logout();
    }
}
