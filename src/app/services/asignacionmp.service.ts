import { Injectable } from '@angular/core';
import {KeyApi} from '../models/KeyApi';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {Observable, throwError} from 'rxjs';
import {PagoDTO} from '../models/Pago';
import {AsignacionPM, AsignacionPMDto} from '../models/AsignacionPM';
import {AcuentaImagenDTO} from '../models/AcuentaImagen';
import {AsignacionImagenDTO} from '../models/AsignacionImagen';

@Injectable({
    providedIn: 'root'
})
export class AsignacionmpService extends KeyApi{

    basePath = VIRACA_API + 'auth/asignacion';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return throwError(
            'Something bad happened; please try again later.');
    }

    createAsignacion(keyApi: string, asignacionPM: AsignacionPM, photos: [any]): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const asignacion = {
            "proyecto_id": asignacionPM.projectId,
            "manager_id": asignacionPM.managerId,
            "monto": asignacionPM.monto,
            "fecha": asignacionPM.fecha,
            "descripcion": asignacionPM.descripcion,
            "photos": photos
        };
        return this.http.post<number>(this.basePath, asignacion, httpOptions);
    }

    getTotalAsigancionByProyectoAndManager(keyApi, proyectoId, managerId){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.post<number>(
            this.basePath + '/get-total-proyecto-manager',
            {"proyecto_id": proyectoId, "manager_id": managerId},
            httpOptions);
    }

    getTotalAsignacionByProyecto(keyApi, proyectoId){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.post<number>(
            this.basePath + '/get-total-proyecto',
            {"proyecto_id": proyectoId},
            httpOptions);
    }

    buscarAsignacionByProyectoManagerFecha(
        keyApi: string, proyectoId: number, managerId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<AsignacionPMDto>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, manager_id: managerId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        return this.http.post<Array<AsignacionPMDto>>(this.basePath + '/search-proyecto-manager-fechas', parametros, httpOptions);
    }

    getImagesAsignacion(keyApi: string, asignacionId: number): Observable<Array<AsignacionImagenDTO>>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Array<AsignacionImagenDTO>>(this.basePath + '/get-images-asignacion/' + asignacionId, httpOptions);
    }

    deleteAsignacion(keyApi: string, asignacionId: number): Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.delete<any>(this.basePath + '/' + asignacionId, httpOptions);
    }
}
