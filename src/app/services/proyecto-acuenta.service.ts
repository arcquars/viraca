import {Injectable} from '@angular/core';
import {KeyApi} from '../models/KeyApi';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {Observable} from 'rxjs';
import {PagoDTO} from '../models/Pago';
import {PagoImagenDTO} from '../models/PagoImagen';
import {AcuentaImagenDTO} from '../models/AcuentaImagen';
import {AcuentaDto} from '../models/Acuenta';

@Injectable({
    providedIn: 'root'
})
export class ProyectoAcuentaService extends KeyApi {

    basePath = VIRACA_API + 'auth/proyecto-pago-acuenta';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    crearAcuenta(keyApi: string, proyecto_id: number, fecha: any, monto: number, descripcion: string, photos: any): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const acuenta = { monto, fecha, proyecto_id, descripcion, photos};
        return this.http.post<number>(this.basePath, acuenta, httpOptions);
    }

    getImagesAcuenta(keyApi: string, acuentaId: number): Observable<Array<AcuentaImagenDTO>>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Array<AcuentaImagenDTO>>(this.basePath + '/get-images-acuenta/' + acuentaId, httpOptions);
    }

    buscarProyectoAcuentaPorFecha(
        keyApi: string, proyectoId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<AcuentaDto>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        return this.http.post<Array<AcuentaDto>>(this.basePath + '/search-proyecto-acuenta-fechas', parametros, httpOptions);
    }

    getTotalAcuentaByProyecto(keyApi, proyectoId){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.post(
            this.basePath + '/get-total-acuenta-proyecto',
            {"proyecto_id": proyectoId},
            httpOptions);
    }
}
