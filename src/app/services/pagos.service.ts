import {Injectable} from '@angular/core';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {Observable, throwError} from 'rxjs';
import {Pago, PagoDTO} from '../models/Pago';
import {KeyApi} from '../models/KeyApi';
import {catchError, retry} from 'rxjs/operators';
import {PagoImagenDTO} from '../models/PagoImagen';

@Injectable({
    providedIn: 'root'
})
export class PagosService extends KeyApi{

    basePath = VIRACA_API + 'auth/pagos';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return throwError(
            'Something bad happened; please try again later.');
    }

    createPago(keyApi: string, pago: PagoDTO): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.post<number>(this.basePath, pago, httpOptions);
    }

    buscarPagosPorFecha(
        keyApi: string, proyectoId: number, personaId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<PagoDTO>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, persona_id: personaId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        return this.http.post<Array<PagoDTO>>(this.basePath + '/get-pagos-by-person-fecha', parametros, httpOptions);
    }

    buscarPagosManagerPorFecha(
        keyApi: string, proyectoId: number, managerId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<PagoDTO>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, manager_id: managerId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        return this.http.post<Array<PagoDTO>>(this.basePath + '/get-pagos-by-manager-fecha', parametros, httpOptions);
    }

    getImagesPago(keyApi: string, pagoId: number): Observable<Array<PagoImagenDTO>>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Array<PagoImagenDTO>>(this.basePath + '/get-pagos-imagen-by-pago-id/' + pagoId, httpOptions);
    }

    deletePago(keyApi: string, pagoId: number): Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.delete<any>(this.basePath + '/' + pagoId, httpOptions);
    }

}
