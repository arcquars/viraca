import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpRequest} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {VIRACA_API, VIRACA_KEY} from '../../environments/environment';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    // API path
    basePath = VIRACA_API + 'auth';

    constructor(private http: HttpClient, private storage: Storage, private plt: Platform) {
    }

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        })
    };

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return throwError(
            'Something bad happened; please try again later.');
    }

    validLogin(user, password): Observable<any>{
        const data = {
            email: user,
            password,
            remenber_me: true
        };
        return this.http.post(this.basePath + '/login', data, this.httpOptions);
    }

    getLock(username): Observable<any>{
        const data = {
            username,
        };
        return this.http.post(this.basePath + '/get-lock-username', data, this.httpOptions).pipe(retry(2), catchError(this.handleError));
    }
}
