import {Injectable} from '@angular/core';
import {VIRACA_API, VIRACA_KEY} from '../../environments/environment';
import {KeyApi} from '../models/KeyApi';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {Storage} from '@ionic/storage';
import {catchError, retry} from 'rxjs/operators';
import {Platform} from '@ionic/angular';
import {PersonaDTO} from '../models/Persona';
import {Manager, ManagerDto} from '../models/Manager';

@Injectable({
    providedIn: 'root'
})
export class ProyectosService extends KeyApi {

    basePath = VIRACA_API + 'auth/proyectos';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            console.log(JSON.stringify(error));
        }
        return throwError(
            'Something bad happened; please try again later.');
    }

    createProyecto(keyApi: string, proyecto: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const body = {nombre: proyecto.nombre, descripcion: proyecto.descripcion,
            presupuesto: proyecto.presupuesto, fecha_inicio: proyecto.fechaInicio, fecha_fin: proyecto.fechaFin};
        return this.http.post(
            this.basePath, body, httpOptions);
    }

    deleteProyecto(keyApi: string, proyecto: number) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        console.log('wwwww:: ' + this.basePath + '/' + proyecto);
        return this.http.delete(this.basePath + '/' + proyecto, httpOptions);
    }

    getProyectos(keyApi: string): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<any>(this.basePath, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    getProyectosByManager(keyApi: string, manager: number){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const body = {user_id: manager};
        return this.http.post(this.basePath + '/get-proyectos-manager', body, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    getProyectoById(keyApi: string, id: number): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.get<any>(this.basePath + '/' + id, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    setPersonProyecto(keyApi: string, persona_id: number, proyecto_id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const body = {persona_id, proyecto_id};
        return this.http.post(
            this.basePath + '/asignar-proyecto-persona', body, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }

    setManagerProyecto(keyApi: string, managerId: number, proyectoId: number) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const body = {user_id: managerId, proyecto_id: proyectoId};
        return this.http.post(
            this.basePath + '/asignar-proyecto-usuario', body, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }

    getPersonasByProyectoId(keyApi: string, id: number): Observable<PersonaDTO[]> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.get<PersonaDTO[]>(this.basePath + '/get-personas/' + id, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    getPersonasByProyectoIdManagerId(keyApi: string, proyectoId: number, managerId: number): Observable<PersonaDTO[]> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        console.log(this.basePath + '/get-personas/' + proyectoId + '/' + managerId);
        return this.http.get<PersonaDTO[]>(
            this.basePath + '/get-personas/' + proyectoId + '/' + managerId, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    getTotalGastosByProyectoId(keyApi: string, id: number): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.get<number>(this.basePath + '/get-total-gastos/' + id, httpOptions);
    }

    getTotalGastosByProyectoIdAndManagerId(keyApi: string, proyectoid: number, managerId): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<number>(this.basePath + '/get-manager-total-gastos/' + proyectoid + '/' + managerId, httpOptions);
    }

    getManagersByProyectoId(keyApi: string, id: number): Observable<Array<ManagerDto>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };

        return this.http.get<Array<ManagerDto>>(this.basePath + '/get-managers/' + id, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }

    getManagersNotProyectoSearch(keyApi: string, proyectoId: number, search: string): Observable<Array<Manager>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const searchObj = {search};
        return this.http.post<Array<Manager>>(
            this.basePath + '/get-managers-no-proyecto/' + proyectoId, searchObj, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }

    removeManagerProyecto(keyApi: string, managerId: number, proyectoId: number) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const body = {manager_id: managerId, proyecto_id: proyectoId};
        return this.http.post(
            this.basePath + '/desasignar-proyecto-usuario', body, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }
}
