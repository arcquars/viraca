import { TestBed } from '@angular/core/testing';

import { ProyectoAcuentaService } from './proyecto-acuenta.service';

describe('ProyectoAcuentaService', () => {
  let service: ProyectoAcuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProyectoAcuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
