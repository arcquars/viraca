import {Injectable} from '@angular/core';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {KeyApi} from '../models/KeyApi';
import {PersonaDTO} from '../models/Persona';

@Injectable({
    providedIn: 'root'
})
export class PersonasService extends KeyApi{

    basePath = VIRACA_API + 'auth/personas';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        })
    };

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            console.log(JSON.stringify(error));
        }

        return throwError(
            'Something bad happened; please try again later.');
    }

    getPersonas(keyApi: string): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<any>(this.basePath, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    createPerson(keyApi: string, person: any): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.post(this.basePath, person, httpOptions);
    }

    updatePerson(keyApi: string, personaId: number, persona: PersonaDTO): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.put(this.basePath + '/' + personaId, persona, httpOptions);
    }

    getPersonById(keyApi: string, id: number): Observable<PersonaDTO> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<PersonaDTO>(this.basePath + '/' + id, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    getPersonasNotProyectoSearch(keyApi: string, proyectoId: number, search: string): Observable<PersonaDTO[]> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const searchObj = {search};
        return this.http.post<PersonaDTO[]>(
            this.basePath + '/get-personas-no-proyecto/' + proyectoId, searchObj, httpOptions)
            .pipe(retry(2), catchError(this.handleError));
    }

}
