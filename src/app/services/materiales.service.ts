import {Injectable} from '@angular/core';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {KeyApi} from '../models/KeyApi';
import {PagoDTO} from '../models/Pago';
import {Observable} from 'rxjs';
import {MaterialDto} from '../models/Material';
import {PagoImagenDTO} from '../models/PagoImagen';
import {MaterialImagenDTO} from '../models/MaterialImagen';

@Injectable({
    providedIn: 'root'
})
export class MaterialesService extends KeyApi {

    basePath = VIRACA_API + 'auth/materiales';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    createMaterial(keyApi: string, material: MaterialDto): Observable<number> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.post<number>(this.basePath, material, httpOptions);
    }

    buscarMaterialesPorFecha(
        keyApi: string, proyectoId: number, personaId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<MaterialDto>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, persona_id: personaId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        return this.http.post<Array<MaterialDto>>(this.basePath + '/get-materials-by-person-fecha', parametros, httpOptions);
    }

    buscarMaterialesManagerPorFecha(
        keyApi: string, proyectoId: number, managerId: number,
        fechaInicio: string, fechaFin: string): Observable<Array<MaterialDto>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        const parametros = {proyecto_id: proyectoId, manager_id: managerId, fecha_inicio: fechaInicio, fecha_fin: fechaFin};
        console.log(JSON.stringify(parametros));
        return this.http.post<Array<MaterialDto>>(this.basePath + '/get-materials-by-manager-fecha', parametros, httpOptions);
    }

    getImagesMaterial(keyApi: string, materialId: number): Observable<Array<MaterialImagenDTO>>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Array<MaterialImagenDTO>>(this.basePath + '/get-materiales-imagen-by-material-id/' + materialId, httpOptions);
    }

    deleteMaterial(keyApi: string, materialId: number): Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.delete<any>(this.basePath + '/' + materialId, httpOptions);
    }
}
