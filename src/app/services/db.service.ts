import { Injectable } from '@angular/core';

import { Platform } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import {
    DATABASE_U_0,
    DATABASE_U_1,
    DATABASE_U_2,
    DATABASE_U_3,
    DATABASE_U_4,
    DATABASE_U_5,
    DATABASE_U_6,
    DATABASE_U_7
} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  private database: SQLiteObject;
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
      private platform: Platform,
      private sqlite: SQLite,
      private sqlitePorter: SQLitePorter,
  ) {
  }

    public openDb() {
      console.log('wwwwwwww 1');
        this.sqlite.create({
            name: 'viraca.db',
            location: 'default'
        })
            .then((db: SQLiteObject) => {
                console.log('wwwwwwww 2');
                this.database = db;
                this.seedDatabase();
                console.log('wwwwwwww 3');

            });
        console.log('wwwwwwww 4');
    }


    seedDatabase() {
        this.getConfigVersionDB().then(resultado => {
            let sql = '';
            let version = 0;
            if (resultado.rows.length > 0) {
                for (let i = 0; i < resultado.rows.length; i++) {
                    version = parseInt(resultado.rows.item(i).u_value, 10);
                }
            }
            if (version === 0) {
                console.log(DATABASE_U_0);
                sql += DATABASE_U_0 + ' ';
                sql += 'insert into config_viraca (u_key, u_value) values (\'database_version\', \'200\'); ';
            }
            if (version === 200) {
                console.log(DATABASE_U_1);
                sql += DATABASE_U_1 + ' ';
                sql += 'update config_viraca set u_value = \'210\' where u_key= \'database_version\';';
            }

            if (version === 210) {
                console.log(DATABASE_U_2);
                sql += DATABASE_U_2 + ' ';
                sql += 'update config_viraca set u_value = \'220\' where u_key= \'database_version\';';
            }

            if (version === 220) {
                console.log(DATABASE_U_3);
                sql += DATABASE_U_3 + ' ';
                sql += 'update config_viraca set u_value = \'230\' where u_key= \'database_version\';';
            }

            if (version === 230) {
                console.log(DATABASE_U_4);
                sql += DATABASE_U_4 + ' ';
                sql += 'update config_viraca set u_value = \'240\' where u_key= \'database_version\';';
            }

            if (version === 240) {
                console.log(DATABASE_U_5);
                sql += DATABASE_U_5 + ' ';
                sql += 'update config_viraca set u_value = \'250\' where u_key= \'database_version\';';
            }

            if (version === 250) {
                console.log(DATABASE_U_6);
                sql += DATABASE_U_6 + ' ';
                sql += 'update config_viraca set u_value = \'260\' where u_key= \'database_version\';';
            }

            if (version === 260) {
                console.log(DATABASE_U_7);
                sql += DATABASE_U_7 + ' ';
                sql += 'update config_viraca set u_value = \'270\' where u_key= \'database_version\';';
            }

            // console.log(sql);
            this.sqlitePorter.importSqlToDb(this.database, sql)
                .then(_ => {
                    this.isDbReady.next(true);
                }, error => {
                    console.log('Error en -> DatabaseService -> seedDatabase -> getConfigVersionDB -> importSqlToDb');
                });
        }, error => {
            console.log('Error en -> DatabaseService -> seedDatabase -> getConfigVersionDB');
            console.log(JSON.stringify(error));
            console.log(DATABASE_U_0);

            let sql = '';
            sql += DATABASE_U_0;
            sql += DATABASE_U_1;
            sql += DATABASE_U_2;
            sql += DATABASE_U_3;
            sql += DATABASE_U_4;
            sql += DATABASE_U_5;
            sql += DATABASE_U_6;
            sql += DATABASE_U_7;
            sql += 'insert into config_viraca (u_key, u_value) values (\'database_version\', \'270\'); ';
            this.sqlitePorter.importSqlToDb(this.database, sql)
                .then(_ => {
                    this.isDbReady.next(true);
                }, error1 => {
                    console.log('Error en -> DatabaseService -> seedDatabase -> getConfigVersionDB -> importSqlToDb111');
                    console.log(JSON.stringify(error1));
                });
        });
    }

    getConfigVersionDB() {
        let sql = 'select * from config_viraca ';
        sql += 'where u_key= ?';
        return this.database.executeSql(sql, ['database_version']);
    }

    getProyectos(){
        const sql = 'SELECT p.*, IFNULL(SUM(pa.monto),0) as total FROM proyectos p ' +
            'LEFT JOIN proyecto_persona pp ON p.id=pp.proyecto_id ' +
            'LEFT JOIN pagos pa ON pa.proyecto_persona_id=pp.id ' +
        'where p.deleted=0 GROUP BY p.id';
        return this.database.executeSql(sql, []);
    }

    crearProyecto(proyecto) {
        const data = [
            proyecto.nombre, proyecto.descripcion,
            proyecto.presupuesto, proyecto.gastos,
            'NUEVO', proyecto.fechaInicio,
            proyecto.fechaFin
        ];

        const insert = 'INSERT INTO proyectos (' +
            'id, nombre, ' +
            'descripcion, presupuesto, ' +
            'gastos, estado, ' +
            'fecha_inicio, fecha_fin, ' +
            'deleted, create_date' +
            ') VALUES (' +
            'null, ?, ' +
            '?, ?, ' +
            '?, ?, ' +
            '?, ?, ' +
            '0, CURRENT_DATE || \' \' || CURRENT_TIME' +
            ')' +
        '';

        return this.database.executeSql(insert, data);
    }

    deleteProyecto(projectId){
        const data = [projectId];
        const sqlDelete = 'update proyectos set deleted = 1 where id= ?';
        return this.database.executeSql(sqlDelete, data);
    }

    getProjectById(proyectId) {
        const sql = 'select * from proyectos where id= ?';
        return this.database.executeSql(sql, [proyectId]);
    }

    crearPerson(person) {
        const data = [
            person.nombres, person.apellidos,
            person.telefono, person.email,
            person.direccion
        ];

        const insert = 'INSERT INTO personas (' +
            'id, nombres, apellidos, ' +
            'telefono, email, ' +
            'direccion, deleted, ' +
            'create_date' +
            ') VALUES (' +
            'null, ?, ?, ' +
            '?, ?, ' +
            '?, 0, ' +
            'CURRENT_DATE || \' \' || CURRENT_TIME' +
            ')' +
            '';

        return this.database.executeSql(insert, data);
    }

    getPersonas(){
        const sql = 'SELECT * FROM personas ' +
            'where deleted=0';
        return this.database.executeSql(sql, []);
    }

    getPersonaById(personaId: number){
        const sql = 'SELECT * FROM personas ' +
            'where id=? AND deleted=0';
        return this.database.executeSql(sql, [personaId]);
    }

    searchPersonas(search: string){

        const sql = 'SELECT * FROM personas ' +
            'where (nombres LIKE ? OR apellidos LIKE ?) AND deleted=0';
        return this.database.executeSql(sql, ['%' + search + '%', '%' + search + '%']);
    }

    deletePersona(personaId){
        const data = [personaId];
        const sqlDelete = 'update personas set deleted = 1 where id= ?';
        return this.database.executeSql(sqlDelete, data);
    }

    addPersonProyect(personId, projectId) {
        const data = [projectId, personId];

        const insert = 'INSERT INTO proyecto_persona (' +
            'id, proyecto_id, persona_id, ' +
            'deleted, create_date' +
            ') VALUES (' +
            'null, ?, ?, ' +
            '0, CURRENT_DATE || \' \' || CURRENT_TIME' +
            ')' +
            '';

        return this.database.executeSql(insert, data);
    }

    getPersonasProyecto(proyectoId: number){
        const sql = 'SELECT p.*, SUM(pago.monto) as total FROM personas p INNER JOIN proyecto_persona pp ' +
            'ON p.id=pp.persona_id LEFT JOIN pagos pago ON pago.proyecto_persona_id =  pp.id ' +
            'WHERE p.deleted=0 AND pp.proyecto_id=? ' +
            'GROUP BY p.id, p.nombres';
        return this.database.executeSql(sql, [proyectoId]);
    }

    getProyectoPersona(proyectoId: number, personaId: number){
        const sql = 'SELECT * FROM proyecto_persona ' +
            'where proyecto_id=? AND persona_id=?';
        return this.database.executeSql(sql, [proyectoId, personaId]);
    }

    addPersonProyectPago(ppId, fechaPago, monto, descripcion) {
        const data = [monto, fechaPago, ppId, descripcion];

        const insert = 'INSERT INTO pagos (' +
            'id, monto, fecha, create_date, ' +
            'proyecto_persona_id, descripcion' +
            ') VALUES (' +
            'null, ?, ?, ' +
            'CURRENT_DATE || \' \' || CURRENT_TIME' +
            ', ?, ?)' +
            '';

        return this.database.executeSql(insert, data);
    }

    addPagoImages(pagoId, url) {
        const insert = 'INSERT INTO pago_imagenes (' +
            'id, url, create_date, ' +
            'pago_id' +
            ') VALUES (' +
            'null, ?, ' +
            'CURRENT_DATE || \' \' || CURRENT_TIME' +
            ', ?)';
        return this.database.executeSql(insert, [url, pagoId]);
    }

    getPagosByProyectoAndPersona(proyectoId: number, personaId: number, dateIni: string, dateFin: string){
        const sql = 'SELECT * FROM pagos ' +
            'where proyecto_persona_id in (' +
            'select id from proyecto_persona where proyecto_id =? AND persona_id=? ' +
            ') AND fecha BETWEEN ? AND ?';
        return this.database.executeSql(sql, [proyectoId, personaId, dateIni, dateFin]);
    }

    deletePagoImagenesByPagoId(pagoId: number){
      const sql = 'DELETE FROM pago_imagenes WHERE pago_id = ?';
      return this.database.executeSql(sql, [pagoId]);
    }

    deletePagosById(pagoId: number){
        const sql = 'DELETE FROM pagos WHERE id = ?';
        return this.database.executeSql(sql, [pagoId]);
    }

    getPagoImagenesByPagoId(pagoId: number){
        const sql = 'SELECT * FROM pago_imagenes ' +
            'where pago_id = ?';
        return this.database.executeSql(sql, [pagoId]);
    }

    getTotalPagoByProyectoId(proyectoId: number){
        const sql = 'select sum(pa.monto) as total from proyectos p inner join ' +
            'proyecto_persona pp on p.id=pp.proyecto_id inner join ' +
            'pagos pa on pp.id=pa.proyecto_persona_id ' +
            'where p.id = ?';
        return this.database.executeSql(sql, [proyectoId]);
    }

    addPersonProyectMaterial(ppId, fecha, cantidad, detalle) {
        const data = [cantidad, fecha, ppId, detalle];

        const insert = 'INSERT INTO materiales (' +
            'id, cantidad, fecha, create_date, ' +
            'proyecto_persona_id, detalle' +
            ') VALUES (' +
            'null, ?, ?, ' +
            'CURRENT_DATE || \' \' || CURRENT_TIME' +
            ', ?, ?)' +
            '';

        return this.database.executeSql(insert, data);
    }

    addMaterialImages(materialId, url) {
        const insert = 'INSERT INTO material_imagenes (' +
            'id, url, create_date, ' +
            'material_id' +
            ') VALUES (' +
            'null, ?, ' +
            'CURRENT_DATE || \' \' || CURRENT_TIME' +
            ', ?)';
        return this.database.executeSql(insert, [url, materialId]);
    }

    getMaterialesByProyectoAndPersona(proyectoId: number, personaId: number, dateIni: string, dateFin: string){
        const sql = 'SELECT * FROM materiales ' +
            'where proyecto_persona_id in (' +
            'select id from proyecto_persona where proyecto_id =? AND persona_id=? ' +
            ') AND fecha BETWEEN ? AND ?';
        return this.database.executeSql(sql, [proyectoId, personaId, dateIni, dateFin]);
    }

    deleteMaterialImagenesByMaterialId(materialId: number){
        const sql = 'DELETE FROM material_imagenes WHERE material_id = ?';
        return this.database.executeSql(sql, [materialId]);
    }

    deleteMaterialesById(materialId: number){
        const sql = 'DELETE FROM materiales WHERE id = ?';
        return this.database.executeSql(sql, [materialId]);
    }

    getMaterialImagenesByMaterialId(materialId: number){
        const sql = 'SELECT * FROM material_imagenes ' +
            'where material_id = ?';
        return this.database.executeSql(sql, [materialId]);
    }
}
