import {AlertController, Platform} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {BehaviorSubject} from 'rxjs';
import {VIRACA_KEY, VIRACA_REMEMBER, VIRACA_ROL, VIRACA_USER_ID, VIRACA_USERNAME} from '../../environments/environment';
import {AuthService} from './auth.service';

const TOKEN_KEY = 'auth-token';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    authenticationState = new BehaviorSubject(false);
    authenticationName = new BehaviorSubject('');
    authenticationRol = new BehaviorSubject('');

    constructor(private storage: Storage, private plt: Platform,
                private authService: AuthService,
                public alertController: AlertController) {
        this.plt.ready().then(() => {
            this.checkToken();
        });
    }

    checkToken() {
        this.storage.get(VIRACA_REMEMBER).then(res => {
            if (res) {
                this.storage.get(VIRACA_USERNAME).then(username => {
                    this.authService.getLock(username).subscribe(result => {
                        console.log(JSON.stringify(result));
                        if (!result.lock) {
                            this.authenticationState.next(true);
                        } else {
                            this.authenticationState.next(false);
                        }
                    }, error => {
                        console.log(JSON.stringify(error));
                        this.errorConectionAlert();
                    });
                });
            } else {
                this.authenticationState.next(false);
            }
        });

        this.storage.get(VIRACA_ROL).then(res => {
            if (res) {
                this.authenticationRol.next(res);
            }
        });
        this.storage.get(VIRACA_USERNAME).then(res => {
            if (res) {
                this.authenticationName.next(res);
            }
        });
    }

    login(username: string, rol: string) {
        return this.storage.set(TOKEN_KEY, 'viraca.arcquars').then(() => {
            this.authenticationState.next(true);
            this.authenticationName.next(username);
            this.authenticationRol.next(rol);
        });
    }

    logout() {
        return this.storage.remove(VIRACA_REMEMBER).then(() => {
            this.storage.remove(VIRACA_USERNAME).then(() => {
                this.storage.remove(VIRACA_USER_ID).then(() => {
                    this.storage.remove(VIRACA_KEY).then(() => {
                        this.authenticationState.next(false);
                        this.authenticationName.next('');
                        this.authenticationRol.next('');
                    });
                });
            });
        });
    }

    isAuthenticated() {
        return this.authenticationState.value;
    }

    async errorConectionAlert() {
        const alert = await this.alertController.create({
            header: 'Sin conexión a internet',
            subHeader: '',
            message: 'Por favor conéctese a internet para usar la aplicación.',
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');

                    }
                }
            ]
        });

        await alert.present();
    }
}
