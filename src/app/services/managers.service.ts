import {Injectable} from '@angular/core';
import {VIRACA_API} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {KeyApi} from '../models/KeyApi';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {Manager} from '../models/Manager';
import {PersonaDTO} from '../models/Persona';

@Injectable({
    providedIn: 'root'
})
export class ManagersService extends KeyApi{

    basePath = VIRACA_API + 'auth/users';

    constructor(private http: HttpClient, public storage: Storage, public plt: Platform) {
        super(storage, plt);
    }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            console.log(JSON.stringify(error));
        }

        return throwError(
            'Something bad happened; please try again later.');
    }

    createManager(keyApi: string, manager: any): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.post(this.basePath, manager, httpOptions);
    }

    getManagers(keyApi: string): Observable<Array<Manager>> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Array<Manager>>(this.basePath, httpOptions).pipe(retry(2), catchError(this.handleError));
    }

    updateLockUserId(keyApi: string, managerId: number): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.post(this.basePath + '/update-lock/' + managerId, {}, httpOptions);
    }

    getMangerById(keyApi: string, id: number): Observable<Manager> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                Authorization: 'Bearer ' + keyApi
            })
        };
        return this.http.get<Manager>(this.basePath + '/' + id, httpOptions).pipe(retry(2), catchError(this.handleError));
    }
}
