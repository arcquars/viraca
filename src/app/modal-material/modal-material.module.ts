import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalMaterialPageRoutingModule } from './modal-material-routing.module';

import { ModalMaterialPage } from './modal-material.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ModalMaterialPageRoutingModule
  ],
  declarations: [ModalMaterialPage]
})
export class ModalMaterialPageModule {}
