import {Component, Input, OnInit} from '@angular/core';
import {AlertController, ModalController, NavParams} from '@ionic/angular';
import {PersonaDTO} from '../models/Persona';
import {DbService} from '../services/db.service';
import {FilePath} from '@ionic-native/file-path/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {File} from '@ionic-native/file/ngx';
import {Proyecto} from '../models/Proyecto';
import {FormBuilder, Validators} from '@angular/forms';
import {ProyectosService} from '../services/proyectos.service';
import {PersonasService} from '../services/personas.service';
import {MaterialesService} from '../services/materiales.service';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY} from '../../environments/environment';
import {MaterialDto} from '../models/Material';
import {LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-modal-material',
    templateUrl: './modal-material.page.html',
    styleUrls: ['./modal-material.page.scss'],
})
export class ModalMaterialPage implements OnInit {

    @Input() personaId: number;
    @Input() proyectoId: number;

    // personId: number;
    // projectId: number;

    persona: PersonaDTO = new PersonaDTO(0, 0, '', '', '', '', '', '', false, 0);
    project: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);

    photos: any = [];
    photosPath: any[] = [];

    errors = new Map<string, string>();

    constructor(private navParams: NavParams,
                public viewCtrl: ModalController,
                private formBuilder: FormBuilder,
                private filePath: FilePath,
                private webview: WebView,
                private storage: Storage,
                private proyectosService: ProyectosService,
                private personaService: PersonasService,
                private materialService: MaterialesService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                public db: DbService, public camera: Camera, public file: File) {
        // this.persona = new PersonaDTO(0, 0, '', '', '', '', '', '', false, 0);
    }

    ionViewWillEnter(){
        this.setPersona();
        this.setProyecto();
    }

    ngOnInit() {
    }

    public registrationForm = this.formBuilder.group({
        fecha: ['', [Validators.required]],
        cantidad: ['', [Validators.required]],
        unidad: ['', [Validators.required]],
        descripcion: ['', []],
    });

    get fecha(){
        return this.registrationForm.get('fecha');
    }

    get cantidad(){
        return this.registrationForm.get('cantidad');
    }

    get unidad(){
        return this.registrationForm.get('unidad');
    }

    get descripcion(){
        return this.registrationForm.get('descripcion');
    }

    // public errorMessages = {
    //     fecha: [
    //         { type: 'required', message: 'Campo es requerido'}
    //     ],
    //     cantidad: [
    //         { type: 'required', message: 'Campo requerido'}
    //     ],
    //     unidad: [
    //         { type: 'required', message: 'Campo requerido'}
    //     ],
    //     descripcion: []
    // };

    dismiss() {
        this.viewCtrl.dismiss();
    }

    async submit(){
        console.log(JSON.stringify(this.registrationForm.value));
        console.log('Listo para grabar!!!!');
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            const material = new MaterialDto(
                0, this.personaId, this.proyectoId,
                this.registrationForm.value.cantidad, this.registrationForm.value.unidad,
                this.registrationForm.value.fecha, this.registrationForm.value.descripcion, this.photos);
            this.materialService.createMaterial(res, material).subscribe(response => {
                loading.dismiss();
                this.dismiss();
            }, (json) => {
                console.log('Errorrrr::::::');
                console.log(JSON.stringify(json));
                this.showErrors(json);
                loading.dismiss();
                this.presentAlertError();
            });
        });
    }

    takePhotos(){
        const options: CameraOptions = {
            quality: 70,
            mediaType: this.camera.MediaType.PICTURE,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG
        };

        this.camera.getPicture().then((imageData) => {
            const filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            const path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            const newBaseFilesystemPath = this.file.dataDirectory;
            this.photosPath.push({filename, path, newBaseFilesystemPath});
            this.file.readAsDataURL(path, filename).then((base64data) => {
                console.log(base64data);
                this.photos.push(base64data);
            });
        });
    }

    savePhotos(materialId){
        this.photosPath.forEach( (photoP) => {
            this.file.copyFile(photoP.path, photoP.filename, photoP.newBaseFilesystemPath, photoP.filename).then( resultPhoto => {
                this.db.addMaterialImages(
                    materialId,
                    this.webview.convertFileSrc(this.file.dataDirectory + photoP.filename)).then( result => {
                    console.log('Grago en la tabla pago_imagenes');
                }, error => {
                    console.log('Error en grabar en table pago imagenes!!!!');
                    console.log(JSON.stringify(error));
                });
            });
        });
    }

    private setPpId(){
        // this.db.getProyectoPersona(this.proyectoId, this.personaId).then(result => {
        //     for (let i = 0; i < result.rows.length; i++) {
        //         this.proyectoPersonaId = result.rows.item(i).id;
        //     }
        // }, error => {
        //     console.log(JSON.stringify(error));
        // });
    }

    private setPersona() {
        console.log('eeeee::: ' + this.personaId);
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personaService.getPersonById(res, this.personaId).subscribe((py) => {
                this.persona = py;
            });
        });
    }

    private setProyecto() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.proyectoId).subscribe((py) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    showErrors(json){
        if ('fecha' in json.error.errors){
            this.errors.set('fecha', json.error.errors.fecha[0]);
        }
        if ('cantidad' in json.error.errors){
            this.errors.set('cantidad', json.error.errors.cantidad[0]);
        }
        if ('unidad' in json.error.errors){
            this.errors.set('unidad', json.error.errors.unidad[0]);
        }
        if ('descripcion' in json.error.errors){
            this.errors.set('descripcion', json.error.errors.descripcion[0]);
        }
    }

    async presentAlertError() {
        const alert = await this.alertController.create({
            header: 'Error',
            message: 'Ocurrio un error al momento de registrar la entrega de material',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.dismiss();
                    }
                }
            ]
        });
        await alert.present();
    }
}
