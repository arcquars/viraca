import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalMaterialPage } from './modal-material.page';

const routes: Routes = [
  {
    path: '',
    component: ModalMaterialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalMaterialPageRoutingModule {}
