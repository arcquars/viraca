import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {DbService} from '../services/db.service';
import {Router} from '@angular/router';
import {PersonasService} from '../services/personas.service';
import {VIRACA_KEY} from '../../environments/environment';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-person',
    templateUrl: './person.page.html',
    styleUrls: ['./person.page.scss'],
})
export class PersonPage implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private storage: Storage,
        private db: DbService,
        private personService: PersonasService,
        private router: Router) {

    }

    registrationPerson = this.formBuilder.group({
        ci: ['', []],
        nombres: ['', [Validators.required, Validators.maxLength(200)]],
        apellido_paterno: ['', [Validators.required, Validators.maxLength(200)]],
        apellido_materno: ['', [Validators.maxLength(200)]],
        telefono: ['', []],
        email: ['', [Validators.email]],
        direccion: ['', []],
    });

    errors = new Map<string, string>();

    get ci() {
        return this.registrationPerson.get('ci');
    }

    get nombres() {
        return this.registrationPerson.get('nombres');
    }

    get apellido_paterno() {
        return this.registrationPerson.get('apellido_paterno');
    }

    get apellido_materno() {
        return this.registrationPerson.get('apellido_materno');
    }

    get telefono() {
        return this.registrationPerson.get('telefono');
    }

    get email() {
        return this.registrationPerson.get('email');
    }

    get direccion() {
        return this.registrationPerson.get('direccion');
    }

    ngOnInit() {
    }


    public submit() {
        console.log(JSON.stringify(this.registrationPerson.value));
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personService.createPerson(res, this.registrationPerson.value).subscribe(response => {
                this.router.navigate(['/persons']);
            }, (json) => {
                this.showErrors(json);
            });
        });
    }

    showErrors(json){
        if ('ci' in json.error.errors){
            this.errors.set('ci', json.error.errors.ci[0]);
        }
        if ('nombres' in json.error.errors){
            this.errors.set('nombres', json.error.errors.nombres[0]);
        }
        if ('apellido_paterno' in json.error.errors){
            this.errors.set('apellido_paterno', json.error.errors.apellido_paterno[0]);
        }
        if ('apellido_materno' in json.error.errors){
            this.errors.set('apellido_materno', json.error.errors.apellido_materno[0]);
        }
        if ('email' in json.error.errors){
            this.errors.set('email', json.error.errors.email[0]);
        }
        if ('telefono' in json.error.errors){
            this.errors.set('telefono', json.error.errors.telefono[0]);
        }
        if ('direccion' in json.error.errors){
            this.errors.set('direccion', json.error.errors.direccion[0]);
        }
    }
}
