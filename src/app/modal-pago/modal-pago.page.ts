import {Component, Input, OnInit} from '@angular/core';
import {AlertController, ModalController, NavParams} from '@ionic/angular';
import {DbService} from '../services/db.service';
import {Persona, PersonaDTO} from '../models/Persona';
import {Proyecto} from '../models/Proyecto';
import {FormBuilder, Validators} from '@angular/forms';

import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {File} from '@ionic-native/file/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../services/proyectos.service';
import {PersonasService} from '../services/personas.service';
import {VIRACA_KEY} from '../../environments/environment';
import {PagosService} from '../services/pagos.service';
import {PagoDTO} from '../models/Pago';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-modal-pago',
    templateUrl: './modal-pago.page.html',
    styleUrls: ['./modal-pago.page.scss'],
})
export class ModalPagoPage implements OnInit {

    personId: number;
    projectId: number;
    person: PersonaDTO = new PersonaDTO(0, 0, '', '', '', '', '', '', false, 0);
    project: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);
    proyectoPersonaId = 0;

    photos: any = [];
    photosPath: any[] = [];

    constructor(private navParams: NavParams,
                public viewCtrl: ModalController,
                private formBuilder: FormBuilder,
                private filePath: FilePath,
                private storage: Storage,
                private proyectosService: ProyectosService,
                private personaService: PersonasService,
                private pagoService: PagosService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                private webview: WebView,
                private router: Router,
                public db: DbService, public camera: Camera, public file: File) {
    }

    ionViewWillEnter() {
        this.personId = this.navParams.get('person_id');
        this.projectId = this.navParams.get('project_id');
        console.log('Perosna id::::: ' + this.personId);
        console.log('Proyecto id::::: ' + this.projectId);
        this.setPersona();
        this.setProyecto();
        this.setPpId();
    }

    registrationForm = this.formBuilder.group({
        fechaPago: ['', [Validators.required]],
        monto: ['', [Validators.required]],
        descripcion: ['', []],
    });

    get fechaPago() {
        return this.registrationForm.get('fechaPago');
    }

    get monto() {
        return this.registrationForm.get('monto');
    }

    get descripcion() {
        return this.registrationForm.get('descripcion');
    }

    public errorMessages = {
        fechaPago: [
            {type: 'required', message: 'Campo es requerido'}
        ],
        monto: [
            {type: 'required', message: 'Campo requerido'}
        ],
        descripcion: []
    };

    ngOnInit() {
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    async submit() {
        console.log(JSON.stringify(this.registrationForm.value));
        console.log('Listo para grabar!!!!');
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            const pago = new PagoDTO(
                0, this.personId, this.projectId,
                this.registrationForm.value.monto, this.registrationForm.value.fechaPago,
                this.registrationForm.value.descripcion, this.photos);
            this.pagoService.createPago(res, pago).subscribe(response => {
                loading.dismiss();
                this.dismiss();
            }, (json) => {
                console.log('Errorrrr::::::');
                console.log(JSON.stringify(json));
                loading.dismiss();
                this.presentAlertError();
                this.showErrors(json);
            });
        });
    }

    showErrors(json){
        // if ('ci' in json.error.errors){
        //     this.errors.set('ci', json.error.errors.ci[0]);
        // }
        // if ('nombres' in json.error.errors){
        //     this.errors.set('nombres', json.error.errors.nombres[0]);
        // }
        // if ('apellido_paterno' in json.error.errors){
        //     this.errors.set('apellido_paterno', json.error.errors.apellido_paterno[0]);
        // }
        // if ('apellido_materno' in json.error.errors){
        //     this.errors.set('apellido_materno', json.error.errors.apellido_materno[0]);
        // }
        // if ('email' in json.error.errors){
        //     this.errors.set('email', json.error.errors.email[0]);
        // }
        // if ('telefono' in json.error.errors){
        //     this.errors.set('telefono', json.error.errors.telefono[0]);
        // }
        // if ('direccion' in json.error.errors){
        //     this.errors.set('direccion', json.error.errors.direccion[0]);
        // }
    }

    private setPersona() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personaService.getPersonById(res, this.personId).subscribe((py) => {
                this.person = py;
            });
        });
    }

    private setProyecto() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    private setPpId() {
        this.db.getProyectoPersona(this.projectId, this.personId).then(result => {
            for (let i = 0; i < result.rows.length; i++) {
                this.proyectoPersonaId = result.rows.item(i).id;
            }
        }, error => {
            console.log(JSON.stringify(error));
        });
    }

    takePhotos() {
        const options: CameraOptions = {
            quality: 70,
            mediaType: this.camera.MediaType.PICTURE,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG
        };

        this.camera.getPicture(options).then((imageData) => {
            const filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            const path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            const newBaseFilesystemPath = this.file.dataDirectory;
            this.photosPath.push({filename, path, newBaseFilesystemPath});
            this.file.readAsDataURL(path, filename).then((base64data) => {
                this.photos.push(base64data);
            });
        });
    }

    async presentAlertError() {
        const alert = await this.alertController.create({
            header: 'Error',
            message: 'Ocurrio un error al momento de registrar el pago',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.dismiss();
                    }
                }
            ]
        });

        await alert.present();
    }
}
