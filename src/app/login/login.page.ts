import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY, VIRACA_REMEMBER, VIRACA_ROL, VIRACA_USER_ID, VIRACA_USERNAME} from '../../environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    public loginForm: FormGroup;
    public errorMessage = '';
    public email = '';
    public password = '';
    public remember = false;

    constructor(private formBuilder: FormBuilder, private authService: AuthenticationService,
                public authSer: AuthService, private storage: Storage) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: new FormControl(this.email, Validators.compose([
                Validators.required
            ])),
            password: new FormControl(this.password, Validators.compose([
                Validators.required
            ])),
            remember: new FormControl(this.remember, Validators.compose([
            ]))
        });
    }

    ionViewWillEnter() {
        this.email = '';
        this.password = '';
        this.remember = false;
    }

    onClickSubmit() {
        this.errorMessage = '';
        console.log('Start login with: '
            + this.loginForm.value.username + ' :: '
            + this.loginForm.value.password + ' :: ' + this.loginForm.value.remember);
        this.authSer.validLogin(this.loginForm.value.username, this.loginForm.value.password).subscribe((response) => {
            this.storage.set(VIRACA_USERNAME, this.loginForm.value.username);
            this.storage.set(VIRACA_ROL, response.role);
            this.storage.set(VIRACA_USER_ID, response.user_id);
            this.storage.set(VIRACA_KEY, response.access_token.plainTextToken.split('|')[1]);
            this.storage.set(VIRACA_REMEMBER, this.loginForm.value.remember);
            this.authService.login(this.loginForm.value.username, response.role);
        }, (error) => {
            console.log('Catch error!!!!!!!!!!!!!!!!!!!!');
            console.log(JSON.stringify(error));
            console.log(error.status);
            if (error.status === 0){
                this.errorMessage = 'No puede conectarse a internet';
            } else{
                this.errorMessage = error.statusText;
            }

        });
    }

}
