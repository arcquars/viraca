import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaterialImagenesPage } from './material-imagenes.page';

const routes: Routes = [
  {
    path: '',
    component: MaterialImagenesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaterialImagenesPageRoutingModule {}
