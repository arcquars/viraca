import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MaterialImagenesPage } from './material-imagenes.page';

describe('MaterialImagenesPage', () => {
  let component: MaterialImagenesPage;
  let fixture: ComponentFixture<MaterialImagenesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialImagenesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MaterialImagenesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
