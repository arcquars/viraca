import {Component, OnInit} from '@angular/core';
import {DbService} from '../services/db.service';
import {ActivatedRoute} from '@angular/router';
import {VIRACA_KEY, VIRACA_URL} from '../../environments/environment';
import {Storage} from '@ionic/storage';
import {MaterialesService} from '../services/materiales.service';
import {MaterialImagenDTO} from '../models/MaterialImagen';

@Component({
    selector: 'app-material-imagenes',
    templateUrl: './material-imagenes.page.html',
    styleUrls: ['./material-imagenes.page.scss'],
})
export class MaterialImagenesPage implements OnInit {

    rootUrl = VIRACA_URL;
    materialId = 0;
    imagenes: Array<MaterialImagenDTO> = [];

    constructor(public db: DbService,
                private storage: Storage,
                private materialService: MaterialesService,
                private route: ActivatedRoute) {
        this.materialId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    }

    ngOnInit() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.materialService.getImagesMaterial(res, this.materialId).subscribe((py) => {
                this.imagenes = py;
            });
        });
    }

}
