import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaterialImagenesPageRoutingModule } from './material-imagenes-routing.module';

import { MaterialImagenesPage } from './material-imagenes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialImagenesPageRoutingModule
  ],
  declarations: [MaterialImagenesPage]
})
export class MaterialImagenesPageModule {}
