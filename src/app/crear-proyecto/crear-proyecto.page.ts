import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DbService} from '../services/db.service';
import {ProyectosService} from '../services/proyectos.service';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY} from '../../environments/environment';

@Component({
  selector: 'app-crear-proyecto',
  templateUrl: './crear-proyecto.page.html',
  styleUrls: ['./crear-proyecto.page.scss'],
})
export class CrearProyectoPage implements OnInit {

  constructor(
      private formBuilder: FormBuilder,
      private db: DbService,
      private storage: Storage,
      private proyectoService: ProyectosService,
      private router: Router
      ) { }

  get nombre() {
    return this.registrationProyecto.get('nombre');
  }

  get descripcion() {
    return this.registrationProyecto.get('descripcion');
  }

  get presupuesto() {
    return this.registrationProyecto.get('presupuesto');
  }

  get fechaInicio() {
    return this.registrationProyecto.get('fechaInicio');
  }

  get fechaFin() {
    return this.registrationProyecto.get('fechaFin');
  }

  registrationProyecto = this.formBuilder.group({
    nombre: ['', [Validators.required, Validators.maxLength(150)]],
    descripcion: ['', [Validators.required, Validators.maxLength(255)]],
    presupuesto: ['', [Validators.required, Validators.min(10), Validators.max(100000)]],
    fechaInicio: ['', [Validators.required]],
    fechaFin: ['', [Validators.required]]
  });

  public errorMessages = {
    nombre: [
      {type: 'required', message: 'Nombre es requerido.'},
      {type: 'maxlength', message: 'Maximo numero de caracteres para nombre es 150'}
    ],
    descripcion: [
      {type: 'required', message: 'Descripcion es requerido'},
      {type: 'maxlength', message: 'Maximo numero de caracteres para descripcion es 255'}
    ],
    presupuesto: [
      {type: 'required', message: 'Presupuesto es requerido'},
      {type: 'min', message: 'Presupuesto no puede ser menor a 10'},
      {type: 'max', message: 'Presupuesto no puede ser mayor a 100000'}
    ],
    fechaInicio: [
      {type: 'required', message: 'Fecha Inicio es requerido'}
    ],
    fechaFin: [
      {type: 'required', message: 'Fecha Fin es requerido'}
    ],
  };

  ngOnInit() {
  }

  public submit() {
    console.log(JSON.stringify(this.registrationProyecto.value));

    this.storage.get(VIRACA_KEY).then((res) => {
      this.proyectoService.createProyecto(res, this.registrationProyecto.value).subscribe( response => {
        this.router.navigateByUrl('/proyecto');
      }, error => {
          console.log('Error al api grabar proyecto!!!');
          console.log(JSON.stringify(error));
      });
    });

    // this.db.crearProyecto(this.registrationProyecto.value).then(() => {
    //   this.router.navigateByUrl('/proyecto');
    // }, (erros) => {
    //   console.log('Error al grabar proyecto!!!');
    //   console.log(JSON.stringify(erros));
    // });
  }
}
