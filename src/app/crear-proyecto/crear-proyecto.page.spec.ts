import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearProyectoPage } from './crear-proyecto.page';

describe('CrearProyectoPage', () => {
  let component: CrearProyectoPage;
  let fixture: ComponentFixture<CrearProyectoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearProyectoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearProyectoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
