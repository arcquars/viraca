import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {DbService} from '../services/db.service';
import {PersonaDTO} from '../models/Persona';
import {PersonasService} from '../services/personas.service';
import {VIRACA_KEY} from '../../environments/environment';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-buscar-persona',
    templateUrl: './buscar-persona.page.html',
    styleUrls: ['./buscar-persona.page.scss'],
})
export class BuscarPersonaPage implements OnInit {

    @Input() proyectoId: number;
    public personList: any[];
    private personas: PersonaDTO[];
    private personId: number;

    constructor(private db: DbService,
                private storage: Storage,
                private personaService: PersonasService,
                public modalController: ModalController) {
    }

    ngOnInit() {
    }

    closeModal() {
        this.modalController.dismiss({personId: this.personId});
    }

    async filterList(evt) {
        const searchTerm = evt.srcElement.value;

        if (!searchTerm) {
            return;
        }

        this.storage.get(VIRACA_KEY).then((res) => {
            this.personaService.getPersonasNotProyectoSearch(res, this.proyectoId, searchTerm).subscribe((py: PersonaDTO[]) => {
                console.log('ssssllll1:::');
                this.personas = py;
                console.log(JSON.stringify(this.personas));
                console.log('ssssllll2:::');
            });
        });
    }

    sendPerson(personId) {
        this.personId = personId;
        this.closeModal();
    }

}
