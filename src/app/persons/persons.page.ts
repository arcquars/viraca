import {Component, OnInit} from '@angular/core';
import {Persona} from '../models/Persona';
import {DbService} from '../services/db.service';
import {AlertController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY} from '../../environments/environment';
import {PersonasService} from '../services/personas.service';

@Component({
    selector: 'app-persons',
    templateUrl: './persons.page.html',
    styleUrls: ['./persons.page.scss'],
})
export class PersonsPage implements OnInit {

    private personas: Persona[] = [];
    private personaBorrar: Persona;

    constructor(private db: DbService,
                private storage: Storage,
                private personaService: PersonasService,
                public alertController: AlertController) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.reloadPersonas();
    }

    deletePersona(personaId) {
        this.db.deleteProyecto(personaId).then((res) => {
                // this.reloadProject();
            },
            (error) => {
                console.log('Error al momento de borrar Proyecto!!!');
                console.log(JSON.stringify(error));
            });

    }

    reloadPersonas() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personaService.getPersonas(res).subscribe((sPersonas: any[]) => {
                this.personas = [];
                console.log('Entro al service personas:::');
                console.log(JSON.stringify(sPersonas));
                console.log('*****************************');
                sPersonas.forEach(sPersona => {
                    this.personas.push(new Persona(
                        sPersona.id, 0, sPersona.nombres, sPersona.apellido_paterno,
                        sPersona.apellido_materno, sPersona.email,
                        sPersona.telefono, sPersona.direccion,
                        (sPersona.deleted === 1)
                    ));
                });
            });
        });
    }

    openConfirmDelete(persona: Persona) {
        this.personaBorrar = persona;
        this.presentAlertConfirm();
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Se borrara a la persona: <b>' + this.personaBorrar.nombre + ' ' + this.personaBorrar.apellidoPaterno + '</b>',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Coonfirmar',
                    handler: () => {
                        this.db.deletePersona(this.personaBorrar.id).then((res) => {
                            this.ionViewWillEnter();
                        }, (error) => {
                            console.log(JSON.stringify(error));
                        });
                    }
                }
            ]
        });

        await alert.present();
    }
}
