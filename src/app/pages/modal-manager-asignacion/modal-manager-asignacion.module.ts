import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalManagerAsignacionPageRoutingModule } from './modal-manager-asignacion-routing.module';

import { ModalManagerAsignacionPage } from './modal-manager-asignacion.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ModalManagerAsignacionPageRoutingModule
  ],
  declarations: [ModalManagerAsignacionPage]
})
export class ModalManagerAsignacionPageModule {}
