import { Component, OnInit } from '@angular/core';
import {Proyecto} from '../../models/Proyecto';
import {Manager} from '../../models/Manager';
import {AlertController, LoadingController, ModalController, NavParams} from '@ionic/angular';
import {FormBuilder, Validators} from '@angular/forms';
import {FilePath} from '@ionic-native/file-path/ngx';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../../services/proyectos.service';
import {DbService} from '../../services/db.service';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {File} from '@ionic-native/file/ngx';
import {ManagersService} from '../../services/managers.service';
import {VIRACA_KEY} from '../../../environments/environment';
import {AsignacionPM} from '../../models/AsignacionPM';
import {AsignacionmpService} from '../../services/asignacionmp.service';

@Component({
    selector: 'app-modal-manager-asignacion',
    templateUrl: './modal-manager-asignacion.page.html',
    styleUrls: ['./modal-manager-asignacion.page.scss'],
})
export class ModalManagerAsignacionPage implements OnInit {

    managerId: number;
    projectId: number;

    manager: Manager;
    project: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);

    photos: any = [];
    photosPath: any[] = [];

    registrationForm: any;
    errorMessages: any;

    constructor(
        private navParams: NavParams,
        public viewCtrl: ModalController,
        private formBuilder: FormBuilder,
        private filePath: FilePath,
        private storage: Storage,
        private proyectosService: ProyectosService,
        public managersSevice: ManagersService,
        public asignacionService: AsignacionmpService,
        public loadingController: LoadingController,
        public alertController: AlertController,
        public db: DbService, public camera: Camera, public file: File
    ) {
        this.registrationForm = this.formBuilder.group({
            fecha: ['', [Validators.required]],
            monto: ['', [Validators.required]],
            descripcion: ['', []],
        });
        this.errorMessages = {
            fecha: [
                {type: 'required', message: 'Campo es requerido'}
            ],
            monto: [
                {type: 'required', message: 'Campo requerido'}
            ],
            descripcion: []
        };
    }

    ionViewWillEnter() {
        this.managerId = this.navParams.get('manager_id');
        this.projectId = this.navParams.get('project_id');
        console.log('Manager id::::: ' + this.managerId);
        console.log('Proyecto id::::: ' + this.projectId);
        this.setManager();
        this.setProyecto();
        // this.setPpId();
    }

    ngOnInit() {
    }

    private setManager(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.managersSevice.getMangerById(res, this.managerId).subscribe((py) => {
                this.manager = py;
            });
        });
    }

    private setProyecto() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    takePhotos() {
        const options: CameraOptions = {
            quality: 70,
            mediaType: this.camera.MediaType.PICTURE,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG
        };

        this.camera.getPicture(options).then((imageData) => {
            const filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            const path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            const newBaseFilesystemPath = this.file.dataDirectory;
            this.photosPath.push({filename, path, newBaseFilesystemPath});
            this.file.readAsDataURL(path, filename).then((base64data) => {
                this.photos.push(base64data);
            });
        });
    }

    get fecha() {
        return this.registrationForm.get('fecha');
    }

    get monto() {
        return this.registrationForm.get('monto');
    }

    get descripcion() {
        return this.registrationForm.get('descripcion');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    async submit() {
        console.log(JSON.stringify(this.registrationForm.value));
        console.log('Listo para grabar!!!!');
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            const asignacionPM = new AsignacionPM(
                0, this.projectId,
                this.managerId,  this.registrationForm.value.monto,
                this.registrationForm.value.fecha, this.registrationForm.value.descripcion,
                false, 0
            );

            this.asignacionService.createAsignacion(res, asignacionPM, this.photos).subscribe(response => {
                loading.dismiss();
                this.dismiss();
            }, error => {
                    console.log('Errorrrr Creando Asignacion::::::');
                    console.log(JSON.stringify(error));
                    loading.dismiss();
                    this.presentAlertError();
                    // this.showErrors(json);
            });
        });
    }

    async presentAlertError() {
        const alert = await this.alertController.create({
            header: 'Error',
            message: 'Ocurrio un error al momento de registrar la asignacion',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.dismiss();
                    }
                }
            ]
        });

        await alert.present();
    }

    roleEsp(role: string){
        return Manager.getRoleEsp(role);
    }
}
