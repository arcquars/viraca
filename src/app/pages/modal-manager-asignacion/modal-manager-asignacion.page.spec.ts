import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalManagerAsignacionPage } from './modal-manager-asignacion.page';

describe('ModalManagerAsignacionPage', () => {
  let component: ModalManagerAsignacionPage;
  let fixture: ComponentFixture<ModalManagerAsignacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalManagerAsignacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalManagerAsignacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
