import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalManagerAsignacionPage } from './modal-manager-asignacion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalManagerAsignacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalManagerAsignacionPageRoutingModule {}
