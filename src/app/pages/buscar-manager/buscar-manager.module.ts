import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuscarManagerPageRoutingModule } from './buscar-manager-routing.module';

import { BuscarManagerPage } from './buscar-manager.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuscarManagerPageRoutingModule
  ],
  declarations: [BuscarManagerPage]
})
export class BuscarManagerPageModule {}
