import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuscarManagerPage } from './buscar-manager.page';

describe('BuscarManagerPage', () => {
  let component: BuscarManagerPage;
  let fixture: ComponentFixture<BuscarManagerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarManagerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuscarManagerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
