import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuscarManagerPage } from './buscar-manager.page';

const routes: Routes = [
  {
    path: '',
    component: BuscarManagerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuscarManagerPageRoutingModule {}
