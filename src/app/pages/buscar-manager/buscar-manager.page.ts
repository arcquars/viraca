import {Component, Input, OnInit} from '@angular/core';
import {PersonaDTO} from '../../models/Persona';
import {Storage} from '@ionic/storage';
import {PersonasService} from '../../services/personas.service';
import {ModalController} from '@ionic/angular';
import {VIRACA_KEY} from '../../../environments/environment';
import {ManagersService} from '../../services/managers.service';
import {Manager} from '../../models/Manager';
import {ProyectosService} from '../../services/proyectos.service';

@Component({
    selector: 'app-buscar-manager',
    templateUrl: './buscar-manager.page.html',
    styleUrls: ['./buscar-manager.page.scss'],
})
export class BuscarManagerPage implements OnInit {

    @Input() proyectoId: number;
    // public personList: any[];
    private managers: Array<Manager> = [];
    private managerId: number;

    constructor(private storage: Storage,
                private proyectosService: ProyectosService,
                public modalController: ModalController) {
    }

    ngOnInit() {
    }

    closeModal() {
        this.modalController.dismiss({managerId: this.managerId});
    }

    async filterList(evt) {
        const searchTerm = evt.srcElement.value;

        if (!searchTerm) {
            return;
        }

        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getManagersNotProyectoSearch(res, this.proyectoId, searchTerm).subscribe((py) => {
                console.log('ssssllll1:::');
                this.managers = py;
                console.log(JSON.stringify(this.managers));
                console.log('ssssllll2:::');
            });
        });
    }

    sendManager(managerId) {
        this.managerId = managerId;
        this.closeModal();
    }

    roleEsp(role: string){
        return Manager.getRoleEsp(role);
    }
}
