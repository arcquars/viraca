import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProyectoAcuentaPage } from './proyecto-acuenta.page';

const routes: Routes = [
  {
    path: '',
    component: ProyectoAcuentaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProyectoAcuentaPageRoutingModule {}
