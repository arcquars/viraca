import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProyectoAcuentaPageRoutingModule } from './proyecto-acuenta-routing.module';

import { ProyectoAcuentaPage } from './proyecto-acuenta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProyectoAcuentaPageRoutingModule
  ],
  declarations: [ProyectoAcuentaPage]
})
export class ProyectoAcuentaPageModule {}
