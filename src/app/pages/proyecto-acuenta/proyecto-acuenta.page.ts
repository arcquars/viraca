import { Component, OnInit } from '@angular/core';
import {ModalPagoProyectoPage} from '../modal-pago-proyecto/modal-pago-proyecto.page';
import {ModalController} from '@ionic/angular';
import {DbService} from '../../services/db.service';
import {Storage} from '@ionic/storage';
import {AuthenticationService} from '../../services/authentication.service';
import {ProyectosService} from '../../services/proyectos.service';
import {Proyecto} from '../../models/Proyecto';
import {ActivatedRoute} from '@angular/router';
import {VIRACA_KEY} from '../../../environments/environment';
import {DatePipe} from '@angular/common';
import {ProyectoAcuentaService} from '../../services/proyecto-acuenta.service';
import {AsignacionmpService} from '../../services/asignacionmp.service';

@Component({
    selector: 'app-proyecto-acuenta',
    templateUrl: './proyecto-acuenta.page.html',
    styleUrls: ['./proyecto-acuenta.page.scss'],
})
export class ProyectoAcuentaPage implements OnInit {

    private key = '';
    private projectId: number;
    private project: Proyecto = null;

    proyecto: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);

    totalAcuenta = 0;
    totalAsignado = 0;
    totalPagos = 0;

    constructor(public modalController: ModalController,
                public datepipe: DatePipe,
                private db: DbService,
                private storage: Storage,
                private route: ActivatedRoute,
                private authService: AuthenticationService,
                private acuentaService: ProyectoAcuentaService,
                private asignacionmp: AsignacionmpService,
                private proyectosService: ProyectosService) {
        this.projectId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.project = new Proyecto(1, '',
            '', 0, 0,
            0, '', '',
            false);
    }

    ngOnInit() {
        this.setProyecto();
        this.setTotales();
    }

    private setProyecto(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py) => {
                this.proyecto = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    private setTotales(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.acuentaService.getTotalAcuentaByProyecto(res, this.projectId).subscribe((py: any) => {
                this.totalAcuenta = py.totalAcuentaProyecto;
            });
            this.proyectosService.getTotalGastosByProyectoId(res, this.projectId).subscribe((result: any) => {
                this.totalPagos = result;
            });
            this.asignacionmp.getTotalAsignacionByProyecto(res, this.projectId).subscribe( (result: any) => {
                this.totalAsignado = result.total;
            });
        });
    }

    async presentAcuentaProyectoModal(proyectoId) {
        const modal = await this.modalController.create({
            component: ModalPagoProyectoPage,
            componentProps: {
                project_id: proyectoId
            }
        });

        modal.onDidDismiss().then(data => {
            this.setTotales();
        });

        return await modal.present();
    }
}
