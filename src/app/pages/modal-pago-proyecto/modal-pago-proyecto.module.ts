import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalPagoProyectoPageRoutingModule } from './modal-pago-proyecto-routing.module';

import { ModalPagoProyectoPage } from './modal-pago-proyecto.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ModalPagoProyectoPageRoutingModule
  ],
  declarations: [ModalPagoProyectoPage]
})
export class ModalPagoProyectoPageModule {}
