import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalPagoProyectoPage } from './modal-pago-proyecto.page';

const routes: Routes = [
  {
    path: '',
    component: ModalPagoProyectoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalPagoProyectoPageRoutingModule {}
