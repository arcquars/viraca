import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController, ModalController, NavParams} from '@ionic/angular';
import {FormBuilder, Validators} from '@angular/forms';
import {FilePath} from '@ionic-native/file-path/ngx';
import {Storage} from '@ionic/storage';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {Router} from '@angular/router';
import {DbService} from '../../services/db.service';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {File} from '@ionic-native/file/ngx';
import {Proyecto} from '../../models/Proyecto';
import {VIRACA_KEY} from '../../../environments/environment';
import {ProyectosService} from '../../services/proyectos.service';
import {ProyectoAcuentaService} from '../../services/proyecto-acuenta.service';

@Component({
    selector: 'app-modal-pago-proyecto',
    templateUrl: './modal-pago-proyecto.page.html',
    styleUrls: ['./modal-pago-proyecto.page.scss'],
})
export class ModalPagoProyectoPage implements OnInit {

    proyectoId: number;

    project: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);

    errors = new Map<string, string>();

    photos: any = [];
    photosPath: any[] = [];

    registrationForm: any;
    errorMessages: any;

    constructor(
        private navParams: NavParams,
        public viewCtrl: ModalController,
        private formBuilder: FormBuilder,
        private filePath: FilePath,
        private storage: Storage,
        private proyectosService: ProyectosService,
        private proyectoAcuentaService: ProyectoAcuentaService,
        public loadingController: LoadingController,
        public alertController: AlertController,
        private webview: WebView,
        private router: Router,
        public db: DbService, public camera: Camera, public file: File
    ) {
        this.registrationForm = this.formBuilder.group({
            fecha: ['', [Validators.required]],
            monto: ['', [Validators.required]],
            descripcion: ['', []],
        });

        this.errorMessages = {
            fecha: [
                {type: 'required', message: 'Campo es requerido'}
            ],
            monto: [
                {type: 'required', message: 'Campo requerido'}
            ],
            descripcion: []
        };
    }

    ionViewWillEnter() {
        this.proyectoId = this.navParams.get('project_id');
        this.setProyecto();
    }

    ngOnInit() {
    }

    get fecha(){
        return this.registrationForm.get('fecha');
    }

    get monto(){
        return this.registrationForm.get('monto');
    }

    get descripcion(){
        return this.registrationForm.get('descripcion');
    }

    public dismiss() {
        this.viewCtrl.dismiss();
    }

    private setProyecto() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.proyectoId).subscribe((py) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    takePhotos() {
        const options: CameraOptions = {
            quality: 70,
            mediaType: this.camera.MediaType.PICTURE,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG
        };

        this.camera.getPicture(options).then((imageData) => {
            const filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            const path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            const newBaseFilesystemPath = this.file.dataDirectory;
            this.photosPath.push({filename, path, newBaseFilesystemPath});
            this.file.readAsDataURL(path, filename).then((base64data) => {
                this.photos.push(base64data);
            });
        });
    }

    async submit(){
        console.log('ProyectoId:: ' + this.proyectoId);
        console.log(JSON.stringify(this.registrationForm.value));
        console.log('Listo para grabar!!!!');
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectoAcuentaService.crearAcuenta(
                res,
                this.proyectoId,
                this.registrationForm.value.fecha,
                this.registrationForm.value.monto,
                this.registrationForm.value.descripcion, this.photos).subscribe( responser => {
                loading.dismiss();
                this.dismiss();
            }, json => {
                console.log('Errorrrr::::::');
                console.log(JSON.stringify(json));
                loading.dismiss();
                this.presentAlertError();
                this.showErrors(json);
            });
        });
    }

    showErrors(json){
        if ('fecha' in json.error.errors){
            this.errors.set('fecha', json.error.errors.fecha[0]);
        }
        if ('monto' in json.error.errors){
            this.errors.set('monto', json.error.errors.cantidad[0]);
        }
        if ('descripcion' in json.error.errors){
            this.errors.set('descripcion', json.error.errors.descripcion[0]);
        }
    }

    async presentAlertError() {
        const alert = await this.alertController.create({
            header: 'Error',
            message: 'Ocurrio un error al momento de registrar el pago a cuenta del proyecto',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.dismiss();
                    }
                }
            ]
        });

        await alert.present();
    }
}
