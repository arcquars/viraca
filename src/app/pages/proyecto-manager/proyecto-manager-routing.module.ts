import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProyectoManagerPage } from './proyecto-manager.page';

const routes: Routes = [
  {
    path: '',
    component: ProyectoManagerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProyectoManagerPageRoutingModule {}
