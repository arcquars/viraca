import {Component, OnInit} from '@angular/core';
import {Proyecto} from '../../models/Proyecto';
import {PersonaDTO} from '../../models/Persona';
import {AlertController, ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../../services/proyectos.service';
import {PersonasService} from '../../services/personas.service';
import {DbService} from '../../services/db.service';
import {VIRACA_KEY, VIRACA_USER_ID} from '../../../environments/environment';
import {ModalPagoPage} from '../../modal-pago/modal-pago.page';
import {ModalMaterialPage} from '../../modal-material/modal-material.page';
import {AsignacionmpService} from '../../services/asignacionmp.service';

@Component({
    selector: 'app-proyecto-manager',
    templateUrl: './proyecto-manager.page.html',
    styleUrls: ['./proyecto-manager.page.scss'],
})
export class ProyectoManagerPage implements OnInit {

    private projectId: number;
    // private managerId: number;
    private userId: number;
    private project: Proyecto = null;
    private personas: Array<PersonaDTO>;
    private gastos = 0;
    private totalAsignado = 0;

    constructor(
        public modalController: ModalController,
        private route: ActivatedRoute,
        private storage: Storage,
        private proyectosService: ProyectosService,
        private personaService: PersonasService,
        private asignacionMP: AsignacionmpService,
        public alertController: AlertController,
        private db: DbService
    ) {
        this.projectId = parseInt(this.route.snapshot.paramMap.get('project_id'), 10);
        this.project = new Proyecto(1, '',
            '', 0, 0,
            0, '', '',
            false);
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        console.log('ProyectoManagerPage ionViewWillEnter!!!!!');
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py: any) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );

            });
            this.storage.get(VIRACA_USER_ID).then((userId) => {
                this.userId = userId;
                this.asignacionMP.getTotalAsigancionByProyectoAndManager(res, this.projectId, userId)
                    .subscribe((result: any) => {
                        this.totalAsignado = result.total;
                    });
                this.getPagosTotal();
                this.loadPersonas();
            });
        });
    }

    loadPersonas(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getPersonasByProyectoIdManagerId(res, this.projectId, this.userId).subscribe((py) => {
                this.personas = py;
            });
        });
    }

    getPagosTotal(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getTotalGastosByProyectoIdAndManagerId(res, this.projectId, this.userId)
                .subscribe( (resultPago: number) => {
                    this.gastos = resultPago;
                });
        });
    }

    async presentModal(personId) {
        const modal = await this.modalController.create({
            component: ModalPagoPage,
            componentProps: {
                person_id: personId,
                project_id: this.projectId
            }
        });

        modal.onDidDismiss().then(data => {
            this.loadPersonas();
            this.getPagosTotal();
        });

        return await modal.present();
    }

    async presentModalMaterial(personId) {
        const modal = await this.modalController.create({
            component: ModalMaterialPage,
            componentProps: {
                personaId: personId,
                proyectoId: this.projectId
            }
        });
        return await modal.present();
    }
}
