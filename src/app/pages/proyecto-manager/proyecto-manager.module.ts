import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ProyectoManagerPageRoutingModule} from './proyecto-manager-routing.module';

import {ProyectoManagerPage} from './proyecto-manager.page';
import {DetailComponent} from '../../proyecto/detail/detail.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProyectoManagerPageRoutingModule
    ],
    exports: [
        DetailComponent
    ],
    declarations: [
        ProyectoManagerPage,
        DetailComponent
    ]
})
export class ProyectoManagerPageModule {
}
