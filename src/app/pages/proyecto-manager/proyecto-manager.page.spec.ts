import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProyectoManagerPage } from './proyecto-manager.page';

describe('ProyectoManagerPage', () => {
  let component: ProyectoManagerPage;
  let fixture: ComponentFixture<ProyectoManagerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoManagerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProyectoManagerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
