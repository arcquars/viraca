import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcuentaImagenesPage } from './acuenta-imagenes.page';

const routes: Routes = [
  {
    path: '',
    component: AcuentaImagenesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcuentaImagenesPageRoutingModule {}
