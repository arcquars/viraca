import { Component, OnInit } from '@angular/core';
import {VIRACA_KEY, VIRACA_URL} from '../../../environments/environment';
import {PagoImagenDTO} from '../../models/PagoImagen';
import {Storage} from '@ionic/storage';
import {ActivatedRoute} from '@angular/router';
import {AcuentaImagenDTO} from '../../models/AcuentaImagen';
import {ProyectoAcuentaService} from '../../services/proyecto-acuenta.service';
import {LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-acuenta-imagenes',
    templateUrl: './acuenta-imagenes.page.html',
    styleUrls: ['./acuenta-imagenes.page.scss'],
})
export class AcuentaImagenesPage implements OnInit {

    rootUrl = VIRACA_URL;
    acuentaId = 0;
    imagenes: Array<AcuentaImagenDTO> = [];

    constructor(
        private storage: Storage,
        private proyectoAcuentaService: ProyectoAcuentaService,
        public loadingController: LoadingController,
        private route: ActivatedRoute) {
        this.acuentaId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        console.log('AcuentaImagenesPage constructor acuentaId::' + this.acuentaId);
    }

    ngOnInit() {
        this.getImagesByAcuentaId();
    }

    async getImagesByAcuentaId(){
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectoAcuentaService.getImagesAcuenta(res, this.acuentaId).subscribe((py) => {
                this.imagenes = py;
                loading.dismiss();
            });
        });
    }
}
