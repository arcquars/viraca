import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcuentaImagenesPage } from './acuenta-imagenes.page';

describe('AcuentaImagenesPage', () => {
  let component: AcuentaImagenesPage;
  let fixture: ComponentFixture<AcuentaImagenesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcuentaImagenesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcuentaImagenesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
