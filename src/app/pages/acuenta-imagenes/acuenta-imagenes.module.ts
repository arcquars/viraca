import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcuentaImagenesPageRoutingModule } from './acuenta-imagenes-routing.module';

import { AcuentaImagenesPage } from './acuenta-imagenes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcuentaImagenesPageRoutingModule
  ],
  declarations: [AcuentaImagenesPage]
})
export class AcuentaImagenesPageModule {}
