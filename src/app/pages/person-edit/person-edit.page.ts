import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators, FormControl, FormGroup} from '@angular/forms';
import {Storage} from '@ionic/storage';
import {DbService} from '../../services/db.service';
import {PersonasService} from '../../services/personas.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Persona, PersonaDTO} from '../../models/Persona';
import {VIRACA_KEY} from '../../../environments/environment';
import {LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-person-edit',
    templateUrl: './person-edit.page.html',
    styleUrls: ['./person-edit.page.scss'],
})
export class PersonEditPage implements OnInit {

    personaId: number;
    persona: PersonaDTO = new PersonaDTO(0, 0, '', '', '', '', '', '', false, 0);

    public registrationUpdatePerson: FormGroup;
    public errors = new Map<string, string>();

    constructor(
        private formBuilder: FormBuilder,
        private storage: Storage,
        private db: DbService,
        private personService: PersonasService,
        private router: Router,
        private route: ActivatedRoute,
        public loadingController: LoadingController
    ) {
        this.personaId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.settingForm();
    }

    ngOnInit() {
        console.log('entro a ngOnInit ..................');
        this.loadPerson();
    }

    async loadPerson(){
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personService.getPersonById(res, this.personaId).subscribe(response => {
                console.log('persona servicio::::::::');
                console.log(JSON.stringify(response));
                this.persona = response;
                this.persona.id = this.personaId;
                this.settingForm();
                loading.dismiss();
            });
        });
    }

    get ci() {
        return this.registrationUpdatePerson.get('ci');
    }

    get nombres() {
        return this.registrationUpdatePerson.get('nombres');
    }

    get apellido_paterno() {
        return this.registrationUpdatePerson.get('apellido_paterno');
    }

    get apellido_materno() {
        return this.registrationUpdatePerson.get('apellido_materno');
    }

    get telefono() {
        return this.registrationUpdatePerson.get('telefono');
    }

    get email() {
        return this.registrationUpdatePerson.get('email');
    }

    get direccion() {
        return this.registrationUpdatePerson.get('direccion');
    }

    public submit() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personService.updatePerson(res, this.personaId, this.registrationUpdatePerson.value).subscribe(response => {
                this.router.navigate(['/persons']);
            }, (json) => {
                this.showErrors(json);
            });
        });
    }

    showErrors(json){
        if ('ci' in json.error.errors){
            this.errors.set('ci', json.error.errors.ci[0]);
        }
        if ('nombres' in json.error.errors){
            this.errors.set('nombres', json.error.errors.nombres[0]);
        }
        if ('apellido_paterno' in json.error.errors){
            this.errors.set('apellido_paterno', json.error.errors.apellido_paterno[0]);
        }
        if ('apellido_materno' in json.error.errors){
            this.errors.set('apellido_materno', json.error.errors.apellido_materno[0]);
        }
        if ('email' in json.error.errors){
            this.errors.set('email', json.error.errors.email[0]);
        }
        if ('telefono' in json.error.errors){
            this.errors.set('telefono', json.error.errors.telefono[0]);
        }
        if ('direccion' in json.error.errors){
            this.errors.set('direccion', json.error.errors.direccion[0]);
        }
    }

    settingForm(){
        this.registrationUpdatePerson = this.formBuilder.group({
            ci: [this.persona.ci, []],
            nombres: [this.persona.nombres, [Validators.required, Validators.maxLength(150)]],
            apellido_paterno: [this.persona.apellido_paterno, [Validators.required, Validators.maxLength(200)]],
            apellido_materno: [this.persona.apellido_materno, [Validators.maxLength(200)]],
            telefono: [this.persona.telefono, [Validators.maxLength(80)]],
            email: [this.persona.email, [Validators.email]],
            direccion: [this.persona.direccion, []],
        });
    }
}
