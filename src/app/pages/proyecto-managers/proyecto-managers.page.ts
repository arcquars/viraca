import {Component, OnInit, ViewChild} from '@angular/core';
import {Proyecto} from '../../models/Proyecto';
import {Manager, ManagerDto} from '../../models/Manager';
import {AlertController, ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../../services/proyectos.service';
import {VIRACA_KEY} from '../../../environments/environment';
import {BuscarManagerPage} from '../buscar-manager/buscar-manager.page';
import {ModalManagerAsignacionPage} from '../modal-manager-asignacion/modal-manager-asignacion.page';
import {DetailComponent} from '../../proyecto/detail/detail.component';

@Component({
    selector: 'app-proyecto-managers',
    templateUrl: './proyecto-managers.page.html',
    styleUrls: ['./proyecto-managers.page.scss'],
})
export class ProyectoManagersPage implements OnInit {

    @ViewChild('appDetail', {static: false}) appDetail: DetailComponent;
    private projectId: number;
    private managers: Array<ManagerDto>;

    constructor(
        public modalController: ModalController,
        private route: ActivatedRoute,
        private storage: Storage,
        private proyectosService: ProyectosService,
        public alertController: AlertController,
    ) {
        this.projectId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    }

    ngOnInit() {

    }

    ionViewWillEnter() {
        this.loadManagers();
    }

    async presentModalFindManager() {
        const modal = await this.modalController.create({
            component: BuscarManagerPage,
            swipeToClose: true,
            cssClass: 'my-custom-class',
            componentProps: {
                proyectoId: this.projectId
            }
        });

        modal.present();

        const data = await modal.onWillDismiss();
        this.addManager(data.data.managerId);

        return await modal.present();
    }

    loadManagers(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getManagersByProyectoId(res, this.projectId).subscribe((py) => {
                this.managers = py;
            });
        });
    }

    addManager(managerId) {
        console.log('xxxx 1:: ' + managerId + ' || proyecto_id:: ' + this.projectId);
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.setManagerProyecto(res, managerId, this.projectId).subscribe((py) => {
                this.loadManagers();
            }, error => {
                console.log('Error add manager:');
                console.log(JSON.stringify(error));
            });
        });
    }

    async presentModalAsignar(managerId) {
        const modal = await this.modalController.create({
            component: ModalManagerAsignacionPage,
            componentProps: {
                manager_id: managerId,
                project_id: this.projectId
            }
        });

        modal.onDidDismiss().then(data => {
            this.loadManagers();
            this.appDetail.getTotalGastoProyecto();
        });

        return await modal.present();
    }

    async presentAlertConfirmManager(managerId, managerName) {
        const alert = await this.alertController.create({
            header: 'Quitar Administrador',
            message: 'Por favor confirme que quitara a <strong>' + managerName + '</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                        console.log('Confirm Okay!!!!');
                        console.log('manager_id:: ' + managerId + ' || project_id:: ' + this.projectId);
                        this.removeManager(managerId);
                    }
                }
            ]
        });

        await alert.present();
    }

    removeManager(managerId) {
        console.log('xxxx remove manager project:: ' + managerId + ' || proyecto_id:: ' + this.projectId);
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.removeManagerProyecto(res, managerId, this.projectId).subscribe((py) => {
                console.log('xxxx 2');
                console.log(JSON.stringify(py));
                this.loadManagers();
            }, error => {
                console.log('Error remove manager :::::::::::::::::');
                console.log(JSON.stringify(error));
            });
        });
    }

    roleEsp(role: string){
        return Manager.getRoleEsp(role);
    }
}
