import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProyectoManagersPage } from './proyecto-managers.page';

const routes: Routes = [
  {
    path: '',
    component: ProyectoManagersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProyectoManagersPageRoutingModule {}
