import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProyectoManagersPageRoutingModule } from './proyecto-managers-routing.module';

import { ProyectoManagersPage } from './proyecto-managers.page';
import {ProyectoManagerPageModule} from '../proyecto-manager/proyecto-manager.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        ProyectoManagersPageRoutingModule,
        ProyectoManagerPageModule
    ],
  declarations: [ProyectoManagersPage]
})
export class ProyectoManagersPageModule {}
