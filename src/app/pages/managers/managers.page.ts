import {Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import {DbService} from '../../services/db.service';
import {ManagersService} from '../../services/managers.service';
import {Router} from '@angular/router';
import {Manager} from '../../models/Manager';
import {VIRACA_KEY} from '../../../environments/environment';
import {Persona} from '../../models/Persona';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-managers',
    templateUrl: './managers.page.html',
    styleUrls: ['./managers.page.scss'],
})
export class ManagersPage implements OnInit {

    private managers: Array<Manager> = [];
    private managerLock: Manager;

    constructor(
        private storage: Storage,
        private managersServices: ManagersService,
        private router: Router,
        public alertController: AlertController
    ) {
    }

    ngOnInit() {
        this.reloadManagers();
    }

    reloadManagers(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.managersServices.getManagers(res).subscribe((sManagers) => {
                this.managers = sManagers;
                console.log('Entro al service directores de proyecto:::');
                console.log(JSON.stringify(sManagers));
                console.log('*****************************');
                console.log(JSON.stringify(this.managers));
            });
        });
    }

    openConfirmLock(manager: Manager) {
        this.managerLock = manager;
        this.presentAlertConfirm();
    }

    async presentAlertConfirm() {
        let message = 'Se bloqueara al administrador: <b>' + this.managerLock.name + '</b>';
        if (this.managerLock.lock) {
            message = 'Se desbloqueara al administrador: <b>' + this.managerLock.name + '</b>';
        }
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        console.log('Confirmar bloqueo de usuario manager!!!!');
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.managersServices.updateLockUserId(res, this.managerLock.id).subscribe((res1) => {
                                console.log('Entro al service directores de proyecto update lock:::');
                                this.reloadManagers();
                            });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    roleEsp(role: string){
        return Manager.getRoleEsp(role);
    }
}
