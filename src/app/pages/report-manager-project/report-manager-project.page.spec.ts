import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportManagerProjectPage } from './report-manager-project.page';

describe('ReportManagerProjectPage', () => {
  let component: ReportManagerProjectPage;
  let fixture: ComponentFixture<ReportManagerProjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportManagerProjectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportManagerProjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
