import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportManagerProjectPage } from './report-manager-project.page';

const routes: Routes = [
  {
    path: '',
    component: ReportManagerProjectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportManagerProjectPageRoutingModule {}
