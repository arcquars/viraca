import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportManagerProjectPageRoutingModule } from './report-manager-project-routing.module';

import { ReportManagerProjectPage } from './report-manager-project.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportManagerProjectPageRoutingModule
  ],
  declarations: [ReportManagerProjectPage]
})
export class ReportManagerProjectPageModule {}
