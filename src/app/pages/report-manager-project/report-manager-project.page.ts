import { Component, OnInit } from '@angular/core';
import {Proyecto} from '../../models/Proyecto';
import {Pago} from '../../models/Pago';
import {Material, MaterialDto} from '../../models/Material';
import {DatePipe} from '@angular/common';
import {AlertController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../../services/proyectos.service';
import {PagosService} from '../../services/pagos.service';
import {MaterialesService} from '../../services/materiales.service';
import {ActivatedRoute} from '@angular/router';
import {VIRACA_KEY} from '../../../environments/environment';
import {ManagersService} from '../../services/managers.service';
import {Manager} from '../../models/Manager';
import {AsignacionmpService} from '../../services/asignacionmp.service';
import {AsignacionPMDto} from '../../models/AsignacionPM';

@Component({
    selector: 'app-report-manager-project',
    templateUrl: './report-manager-project.page.html',
    styleUrls: ['./report-manager-project.page.scss'],
})
export class ReportManagerProjectPage implements OnInit {

    managerId = 0;
    projectId = 0;

    manager: Manager = new Manager();
    proyecto: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);
    pagos: Array<Pago> = [];
    asignacionPMDto: Array<AsignacionPMDto>;
    materiales: Array<Material> = [];
    materialesDto: MaterialDto[];
    total = 0;

    totalAsignado = 0;

    searchFechaIni = '';
    searchFechaFin = '';

    constructor(
        public datepipe: DatePipe,
        public alertController: AlertController,
        private storage: Storage,
        private proyectosService: ProyectosService,
        private managerService: ManagersService,
        private asignacionService: AsignacionmpService,
        private pagoService: PagosService,
        private materialService: MaterialesService,
        private route: ActivatedRoute
    ) {
        this.managerId = parseInt(this.route.snapshot.paramMap.get('managerId'), 10);
        this.projectId = parseInt(this.route.snapshot.paramMap.get('proyectoId'), 10);
        console.log('Datos desde ReportPage:: ' + this.managerId + ' || ' + this.projectId);
    }

    ngOnInit() {
        this.setManager();
        this.setProyecto();
        this.setTotalAsignado();
        const dateNow = new Date();
        this.searchFechaFin = this.datepipe.transform(new Date(dateNow.getTime() + 86400000) , 'yyyy-MM-dd');
        this.searchFechaIni = this.datepipe.transform(new Date(dateNow.setMonth(dateNow.getMonth() - 1)), 'yyyy-MM-dd');
        this.searchAsignaciones();
    }

    private setManager(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.managerService.getMangerById(res, this.managerId).subscribe((py) => {
                this.manager = py;
            });
        });
    }

    private setProyecto(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py) => {
                this.proyecto = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    private setTotalAsignado(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.asignacionService.getTotalAsigancionByProyectoAndManager(res, this.projectId, this.managerId).subscribe( (result: any) => {
                console.log(JSON.stringify(result));
                this.totalAsignado = result.total;
            }, error => {
                console.log(JSON.stringify(error));
            });
        });
    }

    public changeFechaIni(){
        this.searchFechaIni = this.searchFechaIni.split('T')[0];
        console.log('Entro al evento ionChange ini!!!! ' + this.searchFechaIni);
        this.searchAsignaciones();
        // this.searchMateriales();
    }

    public changeFechaFin(){
        this.searchFechaFin = this.searchFechaFin.split('T')[0];
        console.log('Entro al evento ionChange fin!!!! ' + this.searchFechaFin);
        this.searchAsignaciones();
        // this.searchMateriales();
    }

    private searchAsignaciones(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.asignacionService.buscarAsignacionByProyectoManagerFecha(
                res, this.projectId,
                this.managerId, this.searchFechaIni, this.searchFechaFin)
                .subscribe((asignacionPMDto) => {
                    this.asignacionPMDto = asignacionPMDto;
                    this.total = 0;
                    this.asignacionPMDto.forEach(asignacion => {
                        this.total += asignacion.monto;
                    });
                }, error => {
                    console.log('Error en servicios de pagos buscar por rango de fechas...');
                });
        });
    }

    async modalConfirm(asignacionId, fecha){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Esta seguro que quiere eliminar la asignacion con fecha <strong>' + fecha + '</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.asignacionService.deleteAsignacion(res, asignacionId)
                                .subscribe((result) => {
                                    console.log('se borro::: ');
                                    console.log(JSON.stringify(result));
                                    this.searchAsignaciones();
                                    this.setTotalAsignado();
                                }, error => {
                                    console.log('Error en servicios de asignacion borrar ...');
                                    console.log(JSON.stringify(error));
                                });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async modalConfirmDeleteMaterial(materialId, fecha){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Esta seguro que quiere eliminar el material con fecha <strong>' + fecha + '</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.materialService.deleteMaterial(res, materialId)
                                .subscribe((result) => {
                                    console.log('se borro::: ');
                                    console.log(JSON.stringify(result));
                                    this.searchAsignaciones();
                                }, error => {
                                    console.log('Error en servicios de material borrar ...');
                                    console.log(JSON.stringify(error));
                                });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }
}
