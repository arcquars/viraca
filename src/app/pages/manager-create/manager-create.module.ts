import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerCreatePageRoutingModule } from './manager-create-routing.module';

import { ManagerCreatePage } from './manager-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ManagerCreatePage]
})
export class ManagerCreatePageModule {}
