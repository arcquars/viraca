import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManagerCreatePage } from './manager-create.page';

describe('ManagerCreatePage', () => {
  let component: ManagerCreatePage;
  let fixture: ComponentFixture<ManagerCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManagerCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
