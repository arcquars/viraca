import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerCreatePage } from './manager-create.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerCreatePageRoutingModule {}
