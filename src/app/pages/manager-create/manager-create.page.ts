import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Storage} from '@ionic/storage';
import {DbService} from '../../services/db.service';
import {Router} from '@angular/router';
import {VIRACA_KEY} from '../../../environments/environment';
import {ManagersService} from '../../services/managers.service';
import {Manager} from '../../models/Manager';
@Component({
    selector: 'app-manager-create',
    templateUrl: './manager-create.page.html',
    styleUrls: ['./manager-create.page.scss'],
})
export class ManagerCreatePage implements OnInit {

    roleBuilder = Manager.ROLE_BUILDER;
    roleEspBuilder = Manager.ROLE_ESP_BUILDER;
    roleManagerProject = Manager.ROLE_MANAGER_PROJECT;
    roleEspManagerProject = Manager.ROLE_ESP_MANAGER_PROJECT;
    roleSupervisor = Manager.ROLE_SUPERVISOR;
    roleEspSupervisor = Manager.ROLE_ESP_SUPERVISOR;

    constructor(
        private formBuilder: FormBuilder,
        private storage: Storage,
        private db: DbService,
        private managersServices: ManagersService,
        private router: Router
    ) {
        // this.roleBuilder = {Manager.}
        // this.roleBuilder = Manager.ROLE_ESP_SUPERVISOR;
    }

    registrationManager = this.formBuilder.group({
        name: ['', []],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.maxLength(200)]],
        password_confirmation: ['', []],
        role: ['', [Validators.required]]
    });

    errors = new Map<string, string>();

    get name() {
        return this.registrationManager.get('name');
    }

    get email() {
        return this.registrationManager.get('email');
    }

    get password() {
        return this.registrationManager.get('password');
    }

    get password_confirmation() {
        return this.registrationManager.get('password_confirmation');
    }

    get role() {
        return this.registrationManager.get('role');
    }

    public submit() {
        console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy');
        console.log(JSON.stringify(this.registrationManager.value));
        this.storage.get(VIRACA_KEY).then((res) => {
            this.managersServices.createManager(res, this.registrationManager.value).subscribe(response => {
                this.router.navigate(['/managers']);
            }, (json) => {
                console.log(JSON.stringify(json));
                this.showErrors(json);
            });
        });
    }

    showErrors(json){
        if ('name' in json.error.errors){
            this.errors.set('name', json.error.errors.name[0]);
        }
        if ('email' in json.error.errors){
            this.errors.set('email', json.error.errors.email[0]);
        }
        if ('password' in json.error.errors){
            this.errors.set('password', json.error.errors.password[0]);
        }
        if ('password_confirmation' in json.error.errors){
            this.errors.set('password_confirmation', json.error.errors.password_confirmation[0]);
        }
        if ('role' in json.error.errors){
            this.errors.set('role', json.error.errors.role[0]);
        }
    }

    ngOnInit() {
    }

}
