import { Component, OnInit } from '@angular/core';
import {VIRACA_KEY, VIRACA_URL} from '../../../environments/environment';
import {AcuentaImagenDTO} from '../../models/AcuentaImagen';
import {Storage} from '@ionic/storage';
import {ProyectoAcuentaService} from '../../services/proyecto-acuenta.service';
import {LoadingController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {AsignacionmpService} from '../../services/asignacionmp.service';
import {AsignacionImagenDTO} from '../../models/AsignacionImagen';

@Component({
    selector: 'app-asignacion-imagenes',
    templateUrl: './asignacion-imagenes.page.html',
    styleUrls: ['./asignacion-imagenes.page.scss'],
})
export class AsignacionImagenesPage implements OnInit {

    rootUrl = VIRACA_URL;
    asignacionId = 0;
    imagenes: Array<AsignacionImagenDTO> = [];

    constructor(private storage: Storage,
                private asignacionmp: AsignacionmpService,
                public loadingController: LoadingController,
                private route: ActivatedRoute) {
        this.asignacionId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        console.log('AsignacionImagenesPage constructor asignacionId::' + this.asignacionId);
    }

    ngOnInit() {
        this.getImagesByAsignacionId();
    }

    async getImagesByAsignacionId(){
        const loading = await this.loadingController.create({
            message: 'Espera por favor...'
        });
        loading.present();
        this.storage.get(VIRACA_KEY).then((res) => {
            this.asignacionmp.getImagesAsignacion(res, this.asignacionId).subscribe((py) => {
                this.imagenes = py;
                loading.dismiss();
            });
        });
    }
}
