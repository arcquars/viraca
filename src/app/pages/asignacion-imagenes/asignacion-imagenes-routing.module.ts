import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsignacionImagenesPage } from './asignacion-imagenes.page';

const routes: Routes = [
  {
    path: '',
    component: AsignacionImagenesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsignacionImagenesPageRoutingModule {}
