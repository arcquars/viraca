import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsignacionImagenesPageRoutingModule } from './asignacion-imagenes-routing.module';

import { AsignacionImagenesPage } from './asignacion-imagenes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsignacionImagenesPageRoutingModule
  ],
  declarations: [AsignacionImagenesPage]
})
export class AsignacionImagenesPageModule {}
