import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewProyectoPage } from './view-proyecto.page';

describe('ViewProyectoPage', () => {
  let component: ViewProyectoPage;
  let fixture: ComponentFixture<ViewProyectoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProyectoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewProyectoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
