import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewProyectoPage } from './view-proyecto.page';

const routes: Routes = [
  {
    path: '',
    component: ViewProyectoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewProyectoPageRoutingModule {}
