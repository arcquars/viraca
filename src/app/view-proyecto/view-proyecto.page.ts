import {Component, OnInit} from '@angular/core';
import {DbService} from '../services/db.service';
import {ActivatedRoute} from '@angular/router';
import {Proyecto} from '../models/Proyecto';
import {ModalMaterialPage} from '../modal-material/modal-material.page';
import {AlertController, ModalController} from '@ionic/angular';
import {ModalPagoPage} from '../modal-pago/modal-pago.page';
import {BuscarPersonaPage} from '../buscar-persona/buscar-persona.page';
import {PersonaDTO} from '../models/Persona';
import {ProyectosService} from '../services/proyectos.service';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY, VIRACA_ROL} from '../../environments/environment';
import {PersonasService} from '../services/personas.service';
import {BuscarManagerPage} from '../pages/buscar-manager/buscar-manager.page';

@Component({
    selector: 'app-view-proyecto',
    templateUrl: './view-proyecto.page.html',
    styleUrls: ['./view-proyecto.page.scss'],
})
export class ViewProyectoPage implements OnInit {

    private projectId: number;
    private project: Proyecto = null;
    private personas: Array<PersonaDTO>;
    private gastos = 0;

    public isAdmin = false;

    constructor(
        public modalController: ModalController,
        private route: ActivatedRoute,
        private storage: Storage,
        private proyectosService: ProyectosService,
        private personaService: PersonasService,
        public alertController: AlertController,
        private db: DbService
    ) {
        this.projectId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.project = new Proyecto(1, '',
            '', 0, 0,
            0, '', '',
            false);
        this.storage.get(VIRACA_ROL).then((res) => {
            if (res === 'ADMIN') {
                this.isAdmin = true;
            }
        });
    }

    ngOnInit() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py: any) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );

                this.getTotalGastoProyecto();
                this.loadPersonas();
            });
        });
    }

    // ionViewWillEnter() {
    // }

    loadPersonas(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getPersonasByProyectoId(res, this.projectId).subscribe((py) => {
                this.personas = py;
            });
        });
    }

    async presentModal(personId) {
        const modal = await this.modalController.create({
            component: ModalPagoPage,
            componentProps: {
                person_id: personId,
                project_id: this.projectId
            }
        });

        modal.onDidDismiss().then(data => {
            // this.getTotalGastos();
            this.loadPersonas();
            this.getTotalGastoProyecto();
        });

        return await modal.present();
    }

    async presentModalMaterial(personId) {
        const modal = await this.modalController.create({
            component: ModalMaterialPage,
            componentProps: {
                personaId: personId,
                proyectoId: this.projectId
            }
        });
        return await modal.present();
    }

    async presentModalFindPerson() {
        const modal = await this.modalController.create({
            component: BuscarPersonaPage,
            swipeToClose: true,
            cssClass: 'my-custom-class',
            componentProps: {
                proyectoId: this.projectId
            }
        });

        modal.present();

        const data = await modal.onWillDismiss();
        this.addPerson(data.data.personId);

        return await modal.present();
    }

    async presentModalFindManager() {
        const modal = await this.modalController.create({
            component: BuscarManagerPage,
            swipeToClose: true,
            cssClass: 'my-custom-class',
            componentProps: {
                proyectoId: this.projectId
            }
        });

        modal.present();

        const data = await modal.onWillDismiss();
        this.addPerson(data.data.personId);

        return await modal.present();
    }

    addPerson(personId) {
        console.log('xxxx 1:: ' + personId);
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.setPersonProyecto(res, personId, this.projectId).subscribe((py) => {
                console.log('xxxx 2');
                console.log(JSON.stringify(py));
                this.loadPersonas();
            }, error => {
                console.log('Error add person :::::::::::::::::');
                console.log(JSON.stringify(error));
            });
        });
    }

    getTotalGastos(){
        this.db.getTotalPagoByProyectoId(this.projectId).then( resultTotal => {
            this.gastos = 0;
            for (let i = 0; i < resultTotal.rows.length; i++) {
                if (resultTotal.rows.item(i).total !== null) {
                    this.gastos += parseFloat(resultTotal.rows.item(i).total);
                }
            }

            this.gastos = Math.round(this.gastos * 100) / 100;
        }, error => {
            console.log('Entro al error getTotalGastos !!!');
            console.log(JSON.stringify(error));
        });
    }

    getTotalGastoProyecto(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getTotalGastosByProyectoId(res, this.projectId).subscribe((gastos) => {
                this.gastos = gastos;
            });
        });
    }

    async modalConfirmDeletePersona(personaId, personaNombre){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Esta seguro que quiere eliminar a <strong>' + personaNombre + '</strong> (tambien se borrar los pagos y materiales entregados a esta persona por este proyecto)!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            // this.materialService.deleteMaterial(res, materialId)
                            //     .subscribe((result) => {
                            //         console.log('se borro::: ');
                            //         console.log(JSON.stringify(result));
                            //         this.searchPagos();
                            //     }, error => {
                            //         console.log('Error en servicios de material borrar ...');
                            //         console.log(JSON.stringify(error));
                            //     });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }
}
