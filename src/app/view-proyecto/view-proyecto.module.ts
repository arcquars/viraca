import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewProyectoPageRoutingModule } from './view-proyecto-routing.module';

import { ViewProyectoPage } from './view-proyecto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewProyectoPageRoutingModule
  ],
  declarations: [ViewProyectoPage]
})
export class ViewProyectoPageModule {}
