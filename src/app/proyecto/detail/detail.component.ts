import {Component, OnInit} from '@angular/core';
import {Proyecto} from '../../models/Proyecto';
import {VIRACA_KEY} from '../../../environments/environment';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../../services/proyectos.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

    private projectId: number;
    private project: Proyecto = Proyecto.projectEmpty();
    private gastos = 0;

    constructor(private storage: Storage,
                private route: ActivatedRoute,
                private proyectosService: ProyectosService) {
        this.projectId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    }

    ngOnInit() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py: any) => {
                this.project = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );

                this.getTotalGastoProyecto();
            });
        });
    }

    getTotalGastoProyecto(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getTotalGastosByProyectoId(res, this.projectId).subscribe((gastos) => {
                this.gastos = gastos;
            });
        });
    }
}
