import {Component, OnInit} from '@angular/core';
import {DbService} from './../services/db.service';
import {Proyecto} from '../models/Proyecto';
import {AlertController, ModalController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {VIRACA_KEY, VIRACA_ROL, VIRACA_USER_ID} from '../../environments/environment';
import {ProyectosService} from '../services/proyectos.service';
import {AuthenticationService} from '../services/authentication.service';
import {ModalPagoProyectoPage} from '../pages/modal-pago-proyecto/modal-pago-proyecto.page';
import { faUsers, faUsersCog, faToolbox, faShoppingBasket} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-proyecto',
    templateUrl: './proyecto.page.html',
    styleUrls: ['./proyecto.page.scss'],
})
export class ProyectoPage implements OnInit {

    faUsers = faUsers;
    faShoppingBasket = faShoppingBasket;
    faToolbox = faToolbox;
    faUsersCog = faUsersCog;

    private proyectos: Proyecto[] = [];
    private proyectoBorrar: Proyecto;
    private key = '';
    public isAdmin = false;

    constructor(
        private db: DbService,
        private storage: Storage,
        private authService: AuthenticationService,
        private proyectosService: ProyectosService,
        public modalController: ModalController,
        public alertController: AlertController
    ) {
        this.storage.get(VIRACA_ROL).then((res) => {
            if (res === 'ADMIN') {
                this.isAdmin = true;
            }
        });
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        console.log('Proyecto ionViewWillEnter!!!!!');
        this.reloadProject();
    }

    reloadProject() {
        this.storage.get(VIRACA_ROL).then((role) => {
            if (role === 'ADMIN') {
                this.storage.get(VIRACA_KEY).then((res) => {
                    this.proyectosService.getProyectos(res).subscribe((proyectos: any[]) => {
                        this.proyectos = [];
                        for (let i = 0; i < proyectos.length; i++) {
                            const pro = new Proyecto(
                                proyectos[i].id, proyectos[i].nombre,
                                proyectos[i].descripcion, proyectos[i].presupuesto,
                                proyectos[i].acuenta,
                                proyectos[i].gastos, proyectos[i].fecha_inicio,
                                proyectos[i].fecha_fin, proyectos[i].deleted
                            );
                            pro.asignado = proyectos[i].asignado;
                            this.proyectos.push(pro);
                        }
                    });
                });
            } else {
                this.storage.get(VIRACA_KEY).then((res) => {
                    this.storage.get(VIRACA_USER_ID).then((userId) => {
                        this.proyectosService.getProyectosByManager(res, userId).subscribe((proyectos: any[]) => {
                            this.proyectos = [];
                            for (let i = 0; i < proyectos.length; i++) {
                                this.proyectos.push(new Proyecto(
                                    proyectos[i].id, proyectos[i].nombre,
                                    proyectos[i].descripcion, proyectos[i].presupuesto,
                                    proyectos[i].acuenta,
                                    proyectos[i].gastos, proyectos[i].fecha_inicio,
                                    proyectos[i].fecha_fin, proyectos[i].deleted
                                ));
                            }
                        });
                    });
                });
            }
        });
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Se borrara el proyecto: <b>' + this.proyectoBorrar.nombre + '</b>',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.proyectosService.deleteProyecto(res, this.proyectoBorrar.id).subscribe( (result: any) => {
                                console.log('entro a borrar proyecto!!');
                                console.log(JSON.stringify(result));
                                this.reloadProject();
                            }, error => {
                                console.log('Error!!!!');
                                console.log(JSON.stringify(error));
                            });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    openConfirmDelete(proyecto: Proyecto) {
        this.proyectoBorrar = proyecto;
        this.presentAlertConfirm();
    }

    async presentModal(proyectoId) {
        const modal = await this.modalController.create({
            component: ModalPagoProyectoPage,
            componentProps: {
                project_id: proyectoId
            }
        });

        modal.onDidDismiss().then(data => {
            // this.getTotalGastos();
            this.reloadProject();
        });

        return await modal.present();
    }
}
