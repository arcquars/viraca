import { Component } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {ModalPagoPage} from '../modal-pago/modal-pago.page';
import {ModalMaterialPage} from '../modal-material/modal-material.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public modalController: ModalController) {

  }

  async presentModal(personId) {
    const modal = await this.modalController.create({
      component: ModalPagoPage,
      componentProps: {
        person_id : personId
      }
    });
    return await modal.present();
  }

  async presentModalMaterial(personId) {
    const modal = await this.modalController.create({
      component: ModalMaterialPage,
      componentProps: {
        person_id : personId
      }
    });
    return await modal.present();
  }

  addPago(){
    console.log('eeeeeeeeeeeeeeeeeeerrrrrrrrrr:: ');

  }
}
