import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DbService} from '../services/db.service';
import {Persona, PersonaDTO} from '../models/Persona';
import {Proyecto} from '../models/Proyecto';
import {Pago, PagoDTO} from '../models/Pago';
import { DatePipe } from '@angular/common';
import {AlertController} from '@ionic/angular';
import {Material, MaterialDto} from '../models/Material';
import {VIRACA_KEY, VIRACA_ROL} from '../../environments/environment';
import {Storage} from '@ionic/storage';
import {ProyectosService} from '../services/proyectos.service';
import {PersonasService} from '../services/personas.service';
import {PagosService} from '../services/pagos.service';
import {MaterialesService} from '../services/materiales.service';

@Component({
    selector: 'app-report',
    templateUrl: './report.page.html',
    styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

    personId = 0;
    projectId = 0;
    person: PersonaDTO = new PersonaDTO(0, 0, '', '', '', '', '', '', false, 0);
    proyecto: Proyecto = new Proyecto(0, '', '', 0, 0, 0, '', '', false);
    pagos: Array<Pago> = [];
    pagosDto: PagoDTO[];
    materiales: Array<Material> = [];
    total = 0;

    searchFechaIni = '';
    searchFechaFin = '';

    public isAdmin = false;

    constructor(
        public db: DbService,
        public datepipe: DatePipe,
        public alertController: AlertController,
        private storage: Storage,
        private proyectosService: ProyectosService,
        private personaService: PersonasService,
        private pagoService: PagosService,
        private materialService: MaterialesService,
        private route: ActivatedRoute) {
        this.personId = parseInt(this.route.snapshot.paramMap.get('personId'), 10);
        this.projectId = parseInt(this.route.snapshot.paramMap.get('proyectoId'), 10);
        console.log('Datos desde ReportPage:: ' + this.personId + ' || ' + this.projectId);

        this.storage.get(VIRACA_ROL).then((res) => {
            if (res === 'ADMIN') {
                this.isAdmin = true;
            }
        });
    }

    ngOnInit() {
        console.log('Dentro de ngOnInit !!!');
        this.setPersona();
        this.setProyecto();
        const dateNow = new Date();
        this.searchFechaFin = this.datepipe.transform(new Date(dateNow.getTime() + 86400000) , 'yyyy-MM-dd');
        this.searchFechaIni = this.datepipe.transform(new Date(dateNow.setMonth(dateNow.getMonth() - 1)), 'yyyy-MM-dd');
        this.searchPagos();
    }

    private setPersona(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.personaService.getPersonById(res, this.personId).subscribe((py) => {
                this.person = py;
            });
        });
    }

    private setProyecto(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.proyectosService.getProyectoById(res, this.projectId).subscribe((py) => {
                this.proyecto = new Proyecto(
                    py.id, py.nombre,
                    py.descripcion, py.presupuesto,
                    py.acuenta,
                    py.total, py.fecha_inicio,
                    py.fecha_fin, py.deleted
                );
            });
        });
    }

    private searchPagos(){
        this.storage.get(VIRACA_KEY).then((res) => {
            this.pagoService.buscarPagosPorFecha(res, this.projectId, this.personId, this.searchFechaIni, this.searchFechaFin)
                .subscribe((pagosDto) => {
                    this.pagosDto = pagosDto;
                    this.total = 0;
                    this.pagosDto.forEach(pago => {
                        this.total += pago.monto;
                    });
            }, error => {
                console.log('Error en servicios de pagos buscar por rango de fechas...');
            });
        });
    }

    // private searchMateriales(){
    //     this.db.getMaterialesByProyectoAndPersona(
    //         this.projectId,
    //         this.personId,
    //         this.searchFechaIni,
    //         this.searchFechaFin).then( results => {
    //         this.materiales = [];
    //         for (let i = 0; i < results.rows.length; i++) {
    //             let material = new Material();
    //             material.id = parseInt(results.rows.item(i).id, 10);
    //             material.cantidad = parseFloat(results.rows.item(i).cantidad);
    //             material.detalle = results.rows.item(i).detalle;
    //             material.fecha = this.datepipe.transform(new Date(results.rows.item(i).fecha), 'yyyy-MM-dd');
    //             material.proyectoPersona = parseInt(results.rows.item(i).proyecto_persona_id, 10);
    //             this.materiales.push(material);
    //         }
    //     }, error => {
    //         console.log(JSON.stringify(error));
    //     });
    // }

    public changeFechaIni(){
        this.searchFechaIni = this.searchFechaIni.split('T')[0];
        console.log('Entro al evento ionChange ini!!!! ' + this.searchFechaIni);
        this.searchPagos();
    }

    public changeFechaFin(){
        this.searchFechaFin = this.searchFechaFin.split('T')[0];
        console.log('Entro al evento ionChange fin!!!! ' + this.searchFechaFin);
        this.searchPagos();
    }

    async modalConfirm(pagoId, fecha){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Esta seguro que quiere eliminar el pago con fecha <strong>' + fecha + '</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.pagoService.deletePago(res, pagoId)
                                .subscribe((result) => {
                                    console.log('se borro::: ');
                                    console.log(JSON.stringify(result));
                                    this.searchPagos();
                                }, error => {
                                    console.log('Error en servicios de pagos borrar ...');
                                    console.log(JSON.stringify(error));
                                });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async modalConfirmDeleteMaterial(materialId, fecha){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmar!',
            message: 'Esta seguro que quiere eliminar el material con fecha <strong>' + fecha + '</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.storage.get(VIRACA_KEY).then((res) => {
                            this.materialService.deleteMaterial(res, materialId)
                                .subscribe((result) => {
                                    console.log('se borro::: ');
                                    console.log(JSON.stringify(result));
                                    this.searchPagos();
                                }, error => {
                                    console.log('Error en servicios de material borrar ...');
                                    console.log(JSON.stringify(error));
                                });
                        });
                    }
                }
            ]
        });

        await alert.present();
    }
}
