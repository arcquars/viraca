import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagoImagenesPage } from './pago-imagenes.page';

const routes: Routes = [
  {
    path: '',
    component: PagoImagenesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagoImagenesPageRoutingModule {}
