import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagoImagenesPage } from './pago-imagenes.page';

describe('PagoImagenesPage', () => {
  let component: PagoImagenesPage;
  let fixture: ComponentFixture<PagoImagenesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoImagenesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagoImagenesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
