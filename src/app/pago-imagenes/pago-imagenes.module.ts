import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagoImagenesPageRoutingModule } from './pago-imagenes-routing.module';

import { PagoImagenesPage } from './pago-imagenes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagoImagenesPageRoutingModule
  ],
  declarations: [PagoImagenesPage]
})
export class PagoImagenesPageModule {}
