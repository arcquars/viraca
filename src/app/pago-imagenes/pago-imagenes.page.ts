import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PagoImagenDTO} from '../models/PagoImagen';
import {Storage} from '@ionic/storage';
import {PagosService} from '../services/pagos.service';
import {VIRACA_KEY, VIRACA_URL} from '../../environments/environment';

@Component({
    selector: 'app-pago-imagenes',
    templateUrl: './pago-imagenes.page.html',
    styleUrls: ['./pago-imagenes.page.scss'],
})
export class PagoImagenesPage implements OnInit {

    rootUrl = VIRACA_URL;
    pagoId = 0;
    imagenes: Array<PagoImagenDTO> = [];

    constructor(private storage: Storage,
                private pagoService: PagosService, private route: ActivatedRoute) {
        this.pagoId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    }

    ngOnInit() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.pagoService.getImagesPago(res, this.pagoId).subscribe((py) => {
                this.imagenes = py;
            });
        });
    }

}
