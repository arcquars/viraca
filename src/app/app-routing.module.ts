import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
//    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    // redirectTo: 'proyecto',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'report/:personId/:proyectoId',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'person',
    loadChildren: () => import('./person/person.module').then( m => m.PersonPageModule)
  },
  {
    path: 'modal-pago',
    loadChildren: () => import('./modal-pago/modal-pago.module').then( m => m.ModalPagoPageModule)
  },
  {
    path: 'modal-material',
    loadChildren: () => import('./modal-material/modal-material.module').then( m => m.ModalMaterialPageModule)
  },
  {
    path: 'proyecto',
    loadChildren: () => import('./proyecto/proyecto.module').then( m => m.ProyectoPageModule)
  },
  {
    path: 'crear-proyecto',
    loadChildren: () => import('./crear-proyecto/crear-proyecto.module').then( m => m.CrearProyectoPageModule)
  },
  {
    path: 'view-proyecto/:id',
    loadChildren: () => import('./view-proyecto/view-proyecto.module').then( m => m.ViewProyectoPageModule)
  },
  {
    path: 'persons',
    loadChildren: () => import('./persons/persons.module').then( m => m.PersonsPageModule)
  },
  {
    path: 'buscar-persona',
    loadChildren: () => import('./buscar-persona/buscar-persona.module').then( m => m.BuscarPersonaPageModule)
  },
  {
    path: 'pago-imagenes/:id',
    loadChildren: () => import('./pago-imagenes/pago-imagenes.module').then( m => m.PagoImagenesPageModule)
  },
  {
    path: 'material-imagenes/:id',
    loadChildren: () => import('./material-imagenes/material-imagenes.module').then( m => m.MaterialImagenesPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'person-edit/:id',
    loadChildren: () => import('./pages/person-edit/person-edit.module').then( m => m.PersonEditPageModule)
  },
  {
    path: 'buscar-manager',
    loadChildren: () => import('./pages/buscar-manager/buscar-manager.module').then( m => m.BuscarManagerPageModule)
  },
  {
    path: 'managers',
    loadChildren: () => import('./pages/managers/managers.module').then( m => m.ManagersPageModule)
  },
  {
    path: 'manager',
    loadChildren: () => import('./pages/manager/manager.module').then( m => m.ManagerPageModule)
  },
  {
    path: 'manager-create',
    loadChildren: () => import('./pages/manager-create/manager-create.module').then( m => m.ManagerCreatePageModule)
  },
  {
    path: 'proyecto-managers/:id',
    loadChildren: () => import('./pages/proyecto-managers/proyecto-managers.module').then( m => m.ProyectoManagersPageModule)
  },
  {
    path: 'report-manager-projectreport/:managerId/:proyectoId',
    loadChildren: () => import('./pages/report-manager-project/report-manager-project.module').then( m => m.ReportManagerProjectPageModule)
  },
  {
    path: 'modal-manager-asignacion',
    loadChildren: () => import('./pages/modal-manager-asignacion/modal-manager-asignacion.module')
        .then( m => m.ModalManagerAsignacionPageModule)
  },
  {
    path: 'modal-pago-proyecto',
    loadChildren: () => import('./pages/modal-pago-proyecto/modal-pago-proyecto.module').then( m => m.ModalPagoProyectoPageModule)
  },
  {
    path: 'acuenta-imagenes/:id',
    loadChildren: () => import('./pages/acuenta-imagenes/acuenta-imagenes.module').then( m => m.AcuentaImagenesPageModule)
  },
  {
    path: 'asignacion-imagenes/:id',
    loadChildren: () => import('./pages/asignacion-imagenes/asignacion-imagenes.module').then( m => m.AsignacionImagenesPageModule)
  },
  {
    path: 'proyecto-acuenta/:id',
    loadChildren: () => import('./pages/proyecto-acuenta/proyecto-acuenta.module').then( m => m.ProyectoAcuentaPageModule)
  },
  {
    path: 'proyecto-manager/:project_id',
    loadChildren: () => import('./pages/proyecto-manager/proyecto-manager.module').then( m => m.ProyectoManagerPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
