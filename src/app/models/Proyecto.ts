export class Proyecto {
    id: number;
    nombre: string;
    descripcion: string;
    presupuesto: number;
    public asignado: number;
    gastos: number;
    fechaInicio: any;
    fechaFin: any;
    deleted: boolean;
    acuenta: number;

    constructor(
        id: number, nombre: string, descripcion: string,
        presupuesto: number, acuenta: number, gastos: number, fechaInicio: any,
        fechaFin: any, deleted: boolean){
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.acuenta = acuenta;
        this.presupuesto = presupuesto;
        this.gastos = gastos;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.deleted = deleted;
    }

    static projectEmpty(): Proyecto{
        return new Proyecto(1, '',
            '', 0, 0,
            0, '', '',
            false);
    }
}
