export class Material {
    private _id: number;
    private _cantidad: number;
    private _fecha: any;
    private _detalle: string;
    private _proyectoPersona: number;

    constructor(){
        this._id = 0;
        this._cantidad = 0;
        this._fecha = null;
        this._detalle = '';
        this._proyectoPersona = 0;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get cantidad(): number {
        return this._cantidad;
    }

    set cantidad(value: number) {
        this._cantidad = value;
    }

    get fecha(): any {
        return this._fecha;
    }

    set fecha(value: any) {
        this._fecha = value;
    }

    get detalle(): string {
        return this._detalle;
    }

    set detalle(value: string) {
        this._detalle = value;
    }

    get proyectoPersona(): number {
        return this._proyectoPersona;
    }

    set proyectoPersona(value: number) {
        this._proyectoPersona = value;
    }
}

export class MaterialDto {

    id: number;
    persona_id: number;
    proyecto_id: number;
    cantidad: number;
    fecha: any;
    unidad: string;
    descripcion: string;
    photos: [any];

    constructor(id: number, persona_id: number, proyecto_id: number, cantidad: number, unidad: string, fecha: any, descripcion: string, photos: [any]) {
        this.id = id;
        this.persona_id = persona_id;
        this.proyecto_id = proyecto_id;
        this.cantidad = cantidad;
        this.unidad = unidad;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.photos = photos;
    }

}
