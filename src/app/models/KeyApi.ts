import {BehaviorSubject} from 'rxjs';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {VIRACA_KEY} from '../../environments/environment';

export class KeyApi {
    keyState = new BehaviorSubject('');

    constructor(public storage: Storage, public plt: Platform){
        this.plt.ready().then(() => {
            this.checkKey();
        });
    }

    checkKey() {
        this.storage.get(VIRACA_KEY).then((res) => {
            this.keyState.next(res);
        });
    }

    getKey(){
        return this.keyState.value;
    }
}
