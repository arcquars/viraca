export class Manager {

    static readonly ROLE_ADMIN = 'ADMIN';
    static readonly ROLE_BUILDER = 'BUILDER';
    static readonly ROLE_MANAGER_PROJECT = 'MANAGER_PROJECT';
    static readonly ROLE_SUPERVISOR = 'SUPERVISOR';

    static readonly ROLE_ESP_ADMIN = 'ADMINISTRADOR';
    static readonly ROLE_ESP_BUILDER = 'CONSTRUCTOR';
    static readonly ROLE_ESP_MANAGER_PROJECT = 'SUPERVISOR DE OBRA';
    static readonly ROLE_ESP_SUPERVISOR = 'SUPERVISOR';

    public id: number;
    public name: string;
    public email: string;
    public role: string;
    public lock: boolean;

    constructor(){}

    public static getRoleEsp(role): string {
        let roleEsp = '';
        switch (role) {
            case Manager.ROLE_BUILDER:
                roleEsp = Manager.ROLE_ESP_BUILDER;
                break;
            case Manager.ROLE_ADMIN:
                roleEsp = Manager.ROLE_ESP_ADMIN;
                break;
            case Manager.ROLE_MANAGER_PROJECT:
                roleEsp = Manager.ROLE_ESP_MANAGER_PROJECT;
                break;
            case Manager.ROLE_SUPERVISOR:
                roleEsp = Manager.ROLE_ESP_SUPERVISOR;
                break;
        }
        return roleEsp;
    }
}

export class ManagerDto {
    public id: number;
    public name: string;
    public email: string;
    public role: string;
    public pago_proyecto: number;
}
