export class PImagen {
    private _id: number;
    private _url: string;
    private _pagoId: number;


    constructor(id: number, url: string, pagoId: number) {
        this._id = id;
        this._url = url;
        this._pagoId = pagoId;
    }


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get url(): string {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }

    get pagoId(): number {
        return this._pagoId;
    }

    set pagoId(value: number) {
        this._pagoId = value;
    }
}
