export class AcuentaImagenDTO {
    id: number;
    url: string;
    acuenta_id: number;

    constructor(id, url, acuentaId){
        this.id = id;
        this.url = url;
        this.acuenta_id = acuentaId;
    }
}
