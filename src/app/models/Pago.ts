export class Pago {
    private _id: number;
    private _monto: number;
    private _fecha: any;
    private _descripcion: string;
    private _proyectoPersona: number;


    constructor(id: number, monto: number, fecha: any, descripcion: string, proyectoPersona: number) {
        this._id = id;
        this._monto = monto;
        this._fecha = fecha;
        this._descripcion = descripcion;
        this._proyectoPersona = proyectoPersona;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get monto(): number {
        return this._monto;
    }

    set monto(value: number) {
        this._monto = value;
    }

    get fecha(): any {
        return this._fecha;
    }

    set fecha(value: any) {
        this._fecha = value;
    }

    get proyectoPersona(): number {
        return this._proyectoPersona;
    }

    set proyectoPersona(value: number) {
        this._proyectoPersona = value;
    }

    get descripcion(): string {
        return this._descripcion;
    }

    set descripcion(value: string) {
        this._descripcion = value;
    }
}

export class PagoDTO {
    id: number;
    persona_id: number;
    proyecto_id: number;
    monto: number;
    fecha: any;
    descripcion: string;
    photos: [any];

    constructor(id: number, persona_id: number, proyecto_id: number, monto: number, fecha: any, descripcion: string, photos: [any]) {
        this.id = id;
        this.persona_id = persona_id;
        this.proyecto_id = proyecto_id;
        this.monto = monto;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.photos = photos;
    }
}
