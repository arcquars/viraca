export class AsignacionPM{

    constructor(
        public id: number, public projectId: number,
        public managerId: number, public monto: number,
        public fecha: string, public descripcion: string,
        public deleted: boolean, public userId: number
        ){
    }
}

export class AsignacionPMDto {

    constructor(
        public id: number,
        public proyecto_id: number,
        public manager_id: number,
        public monto: number,
        public fecha: string,
        public descripcion: string
    ) {
    }
}
