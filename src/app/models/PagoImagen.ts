export class PagoImagenDTO {
    id: number;
    url: string;
    pago_id: number;

    constructor(id, url, pagoId){
        this.id = id;
        this.url = url;
        this.pago_id = pagoId;
    }
}
