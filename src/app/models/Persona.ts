export class Persona {
    id: number;
    ci: number;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    email: string;
    telefono: string;
    direccion: string;
    deleted: boolean;
    private _totalPagos: number;

    constructor(
        id: number, ci: number, nombre: string, apellidoPaterno: string, apellidoMaterno: string,
        email: string, telefono: string, direccion: string, deleted: boolean
    ){
        this.id = id;
        this.ci = ci;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.email = email;
        this. telefono = telefono;
        this.direccion = direccion;
        this.deleted = deleted;
    }

    get totalPagos(): number {
        return this._totalPagos;
    }

    set totalPagos(value: number) {
        this._totalPagos = value;
    }

    nombreCompleto(): string{
        return this.nombre + ' ' + this.apellidoPaterno + ' ' + this.apellidoMaterno;
    }
}

export class PersonaDTO {
    id: number;
    ci: number;
    nombres: string;
    apellido_paterno: string;
    apellido_materno: string;
    email: string;
    telefono: string;
    direccion: string;
    deleted: boolean;
    pago_proyecto: number;

    constructor(
        id: number, ci: number, nombres: string, apellido_paterno: string, apellido_materno: string,
        email: string, telefono: string, direccion: string, deleted: boolean, pago_proyecto: number
    ){
        this.id = id;
        this.ci = ci;
        this.nombres = nombres;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.email = email;
        this. telefono = telefono;
        this.direccion = direccion;
        this.pago_proyecto = pago_proyecto;
        this.deleted = deleted;
    }
}
