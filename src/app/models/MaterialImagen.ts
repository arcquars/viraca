export class MaterialImagenDTO {
    id: number;
    url: string;
    material_id: number;

    constructor(id, url, materialId){
        this.id = id;
        this.url = url;
        this.material_id = materialId;
    }
}
