export class AsignacionImagenDTO {
    id: number;
    url: string;
    asignacion_id: number;

    constructor(id, url, asignacionId){
        this.id = id;
        this.url = url;
        this.asignacion_id = asignacionId;
    }
}
