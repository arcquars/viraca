CREATE TABLE IF NOT EXISTS proyectos(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nombre TEXT,
    descripcion TEXT,
    fecha_i date,
    fecha_f date,
    presupuesto real default 0,
    gasto real default 0,
    deleted boolean default 0
);

INSERT or IGNORE INTO proyectos(id, nombre, descripcion, fecha_i, fecha_f, presupuesto, gasto, deleted) VALUES
(null, 'Proyecto 1', 'Descripcion del proyecto 1', '12-12-2020', '11-12-2020', 8000, 522, 0);
