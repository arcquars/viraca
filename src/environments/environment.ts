// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

export const VIRACA_API = 'http://192.168.1.102:8001/api/';
export const VIRACA_URL = 'http://192.168.1.102:8001';
// export const VIRACA_API = 'https://arc.lugubria.net/api/';
// export const VIRACA_URL = 'https://arc.lugubria.net/';
export const VIRACA_USERNAME = 'viraca_username';
export const VIRACA_ROL = 'viraca_rol';
export const VIRACA_USER_ID = 'viraca_user_id';
export const VIRACA_KEY = 'viraca_key';
export const VIRACA_REMEMBER = 'viraca_remember';

export const VERSION_APP = '2.0.6';
export const DATABASE_U_0 = '' +
    'CREATE TABLE IF NOT EXISTS config_viraca ( ' +
    '    id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
    '    u_key text not null unique, ' +
    '    u_value text not null ' +
    '); ' +
    'CREATE TABLE IF NOT EXISTS proyectos(' +
    '  id INTEGER PRIMARY KEY,' +
    '  nombre text NOT NULL,' +
    '  descripcion text NULL,' +
    '  presupuesto float default 0,' +
    '  gastos float default 0,' +
    '  estado text null,' +
    '  fecha_inicio date null,' +
    '  fecha_fin date null,' +
    '  deleted boolean default 0,' +
    '  create_date text); ' +
    'INSERT INTO proyectos(' +
    '   id, nombre, descripcion, presupuesto, ' +
    '   gastos, estado, fecha_inicio, fecha_fin, deleted, create_date) ' +
    'VALUES (' +
    '   null, \'Proytecto 14\', \'Pequenia descripcion 14\', 2000, ' +
    '   0, \'NUEVO\', null, null, 0, CURRENT_DATE || \' \' || CURRENT_TIME); ' +
    'INSERT INTO proyectos(' +
    '   id, nombre, descripcion, presupuesto, ' +
    '   gastos, estado, fecha_inicio, fecha_fin, deleted, create_date) ' +
    'VALUES(' +
    '   null, \'Proytecto 15\', \'Pequenia descripcion 15\', 2500, ' +
    '   30, \'NUEVO\', null, null, 0, CURRENT_DATE || \' \' || CURRENT_TIME); ' +
    'INSERT INTO proyectos(' +
    '   id, nombre, descripcion, presupuesto, ' +
    '   gastos, estado, fecha_inicio, fecha_fin, deleted, create_date) ' +
    'VALUES(' +
    '   null, \'Proytecto 16\', \'Pequenia descripcion 16\', 3000, ' +
    '   56, \'NUEVO\', null, null, 0, CURRENT_DATE || \' \' || CURRENT_TIME); ' +
    '';

export const DATABASE_U_1 = '' +
    'CREATE TABLE IF NOT EXISTS personas(' +
    'id INTEGER PRIMARY KEY, ' +
    'nombres text NOT NULL, ' +
    'apellidos text NULL, ' +
    'telefono text null, ' +
    'email text null, ' +
    'direccion text null, ' +
    'deleted boolean default 0, ' +
    'create_date text' +
    '); ' +
    'CREATE TABLE IF NOT EXISTS proyecto_persona(' +
    'id INTEGER PRIMARY KEY, ' +
    'proyecto_id integer NOT NULL, ' +
    'persona_id integer NULL, ' +
    'deleted boolean default 0, ' +
    'create_date text); ' +
    '';

export const DATABASE_U_2 = '' +
    'CREATE TABLE IF NOT EXISTS proyecto_materia(' +
    'id INTEGER PRIMARY KEY, proyecto_id INTEGER NOT NULL, ' +
    'persona_id INTEGER NOT NULL, detalle TEXT NOT NULL, ' +
    'cantidad REAL NOT NULL, fecha_entrega TEXT NOT NULL, ' +
    'deleted BOOLEAN default 0, create_date TEXT);';


export const DATABASE_U_3 = '' +
    'CREATE TABLE IF NOT EXISTS pagos(' +
    'id INTEGER PRIMARY KEY, monto REAL NOT NULL, ' +
    'fecha date null, create_date TEXT, ' +
    'proyecto_persona_id INTENGER NOT NULL, ' +
    'FOREIGN KEY(proyecto_persona_id) ' +
    'REFERENCES proyecto_persona (proyecto_persona_id)); ' +
    'CREATE TABLE IF NOT EXISTS entrega_materiales(' +
    'id INTEGER PRIMARY KEY, detalle TEXT NOT NULL, ' +
    'cantidad INTEGER NOT NULL, fecha date null, ' +
    'create_date TEXT, proyecto_persona_id INTENGER NOT NULL, ' +
    'FOREIGN KEY(proyecto_persona_id) REFERENCES ' +
    'proyecto_persona (proyecto_persona_id));';

export const DATABASE_U_4 = '' +
    'ALTER TABLE pagos ADD COLUMN descripcion TEXT;';

export const DATABASE_U_5 = '' +
    'CREATE TABLE IF NOT EXISTS pago_imagenes(' +
    'id INTEGER PRIMARY KEY, url TEXT NOT NULL, ' +
    'create_date TEXT, pago_id INTENGER NOT NULL, ' +
    'FOREIGN KEY(pago_id) REFERENCES pagos (pago_id));';

export const DATABASE_U_6 = '' +
    'CREATE TABLE IF NOT EXISTS materiales(' +
    'id INTEGER PRIMARY KEY, cantidad REAL NOT NULL, ' +
    'detalle TEXT, ' +
    'fecha date null, create_date TEXT, ' +
    'proyecto_persona_id INTENGER NOT NULL, ' +
    'FOREIGN KEY(proyecto_persona_id) ' +
    'REFERENCES proyecto_persona (proyecto_persona_id)); ';

export const DATABASE_U_7 = '' +
    'CREATE TABLE IF NOT EXISTS material_imagenes(' +
    'id INTEGER PRIMARY KEY, url TEXT NOT NULL, ' +
    'create_date TEXT, material_id INTENGER NOT NULL, ' +
    'FOREIGN KEY(material_id) REFERENCES materiales (material_id)); ';
